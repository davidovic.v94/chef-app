# ChefApp
> Android application for cooking.

## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Status](#status)
* [Inspiration](#inspiration)

## General info
ChefApp uses Spoonacular API which is food API with 5000+ recipes. Spoonacular API is free and it provides lots of API calls (about recipes, ingredients, meal types, nutrition, meal plans, wine and other).

Application is devided into several parts:
* Home - Recommended recipes based on favorites.
* Find recipe - Search for particular recipe (also contains filter).
* What's in your fridge - Add all ingredients that you have and search recipes.
* Have no idea? - Spin the "food wheel" and find recipes.
* Ask me something - Get the answer for particular question ("How much vitamin c is in 2 apples?").
* Did you know - Get random trivia about food.
* Favorites - List of favorite recipes.

Application also contains recipe details where you can find summary, ready time, ingredients, equipement, steps, and other details like taste, nutrition and price which are shown as charts.

Recipes can be shared.

## Screenshots

<img src="./images/Screenshot_20210120_132237.png" width="100">
<img src="./images/Screenshot_20210120_132332.png" width="100">
<img src="./images/Screenshot_20210120_132410.png" width="100">
<img src="./images/Screenshot_20210120_132449.png" width="100">
<img src="./images/Screenshot_20210120_132512.png" width="100">
<img src="./images/Screenshot_20210120_132542.png" width="100">
<img src="./images/Screenshot_20210120_132628.png" width="100">
<img src="./images/Screenshot_20210120_132645.png" width="100">
<img src="./images/Screenshot_20210120_132732.png" width="100">
<img src="./images/Screenshot_20210120_132755.png" width="100">
<img src="./images/Screenshot_20210120_132817.png" width="100">
<img src="./images/Screenshot_20210120_132918.png" width="100">
<img src="./images/Screenshot_20210120_133013.png" width="100">
<img src="./images/Screenshot_20210120_133325.png" width="100">
<img src="./images/Screenshot_20210120_133353.png" width="100">
<img src="./images/Screenshot_20210120_133442.png" width="100">
<img src="./images/Screenshot_20210120_133502.png" width="100">
<img src="./images/Screenshot_20210120_133519.png" width="100">
<img src="./images/Screenshot_20210120_133534.png" width="100">
<img src="./images/Screenshot_20210120_133552.png" width="100">
<img src="./images/Screenshot_20210120_133614.png" width="100">
<img src="./images/Screenshot_20210120_133703.png" width="100">
<img src="./images/Screenshot_20210120_133721.png" width="100">
<img src="./images/Screenshot_20210120_133854.png" width="100">
<img src="./images/Screenshot_20210120_133923.png" width="100">

## Technologies
Project is created with:

* Java 1.8
* Gradle 6.5

Libraries:

* MaterialDesign 1.2.1
* Lombok 1.18.14
* Picasso 2.71828
* MPAndroidChart 3.1.0
* Recyclerview-swipedecorator 1.2.3
* Konfetti 1.2.5
* Gson 2.8.6

## Setup
To run this project, download and start it in android studio.

## Status
The project was made for learning purposes. It's not tested on real android device.

## Inspiration
As I found Spoonacular API I saw great opportunity to make food app. This was the result.
