package com.vojkan.chefapp.recipedata;

import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONObject;

import java.util.Set;

import lombok.Data;

@Data
public class Instruction {

    private Integer stepNumber;
    private String step;
    private Set<Ingredient> ingredients;
    private Set<Equipment> equipments;
    private Integer length;
    private String unit = "min";

    public Instruction(JSONObject instructionJSON) {
        try {
            this.stepNumber = (Integer) JSONNullChecker.checkForNullValueField(instructionJSON,"number");
            this.step = String.valueOf(JSONNullChecker.checkForNullValueField(instructionJSON,"step"));
//            this.length = (int) JSONNullChecker.checkForNullValueField(instructionJSON,"length");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
