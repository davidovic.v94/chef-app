package com.vojkan.chefapp.recipedata.enums;

public enum MealType {
    MAIN_COURSE,
    SIDE_DISH,
    DESSERT,
    APPETIZER,
    SALAD,
    BREAD,
    BREAKFAST,
    SOUP,
    BEVERAGE,
    SAUCE,
    MARINADE,
    FINGER_FOOD,
    SNACK,
    DRINK
}
