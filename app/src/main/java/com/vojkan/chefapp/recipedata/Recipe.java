package com.vojkan.chefapp.recipedata;

import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class Recipe {

    private Integer id;
    private String name;
    private String imageUrl;
    private String recipeUrl;
    private Integer servings;
    private String time;
    private Double healthScore;
    private List<String> cuisines;
    private List<String> dishTypes;
    private List<String> diets;
    private Boolean dairyFree;
    private Boolean glutenFree;
    private Boolean ketogenic;
    private Boolean vegan;
    private Boolean lowFodmap;
    private Boolean sustainable;
    private Boolean vegetarian;
    private Boolean veryHealthy;
    private Boolean veryPopular;
    private Boolean whole30;
    private Set<Nutrient> nutrients;
    private Set<Ingredient> ingredients;
    private String summary;
    private String instructions;
    private Set<Instruction> analyzedInstructions;
    private Double pricePerServing;
    private Double totalPrice;

    public Recipe() {

    }

    public Recipe(JSONObject recipeJSON) {
        try {
            this.id = (Integer) JSONNullChecker.checkForNullValueField(recipeJSON, "id");
            this.name = String.valueOf(JSONNullChecker.checkForNullValueField(recipeJSON, "title"));
            this.imageUrl = (String) JSONNullChecker.checkForNullValueField(recipeJSON, "image");
            this.recipeUrl = (String) JSONNullChecker.checkForNullValueField(recipeJSON, "spoonacularSourceUrl");
            this.servings = (Integer) JSONNullChecker.checkForNullValueField(recipeJSON,"servings");
            this.time = String.valueOf(JSONNullChecker.checkForNullValueField(recipeJSON,"readyInMinutes"));
            this.healthScore = (Double) JSONNullChecker.checkForNullValueField(recipeJSON,"healthScore");
            this.cuisines = Collections.singletonList(recipeJSON.getString("cuisines"));
            this.dishTypes = Collections.singletonList(recipeJSON.getString("dishTypes"));
            this.diets = Collections.singletonList(recipeJSON.getString("diets"));
            this.dairyFree = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"dairyFree");
            this.glutenFree = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"glutenFree");
            this.ketogenic = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"ketogenic");
            this.vegan = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"vegan");
            this.lowFodmap = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"lowFodmap");
            this.sustainable = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"sustainable");
            this.vegetarian = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"vegetarian");
            this.veryHealthy = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"veryHealthy");
            this.veryPopular = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"veryPopular");
            this.whole30 = (Boolean) JSONNullChecker.checkForNullValueField(recipeJSON,"whole30");
            this.summary = String.valueOf(JSONNullChecker.checkForNullValueField(recipeJSON,"summary"));
            this.instructions = String.valueOf(JSONNullChecker.checkForNullValueField(recipeJSON,"instructions"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
