package com.vojkan.chefapp.recipedata.enums;

public enum Diet {
    GLUTEN_FREE,
    KETOGENIC,
    VEGETARIAN,
    LACTO_VEGETARIAN,
    OVO_VEGETARIAN,
    VEGAN,
    PESCETARIAN,
    PALEO,
    PRIMAL,
    WHOLE30
}
