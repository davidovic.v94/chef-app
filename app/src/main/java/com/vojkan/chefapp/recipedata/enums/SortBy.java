package com.vojkan.chefapp.recipedata.enums;

public enum SortBy {
    NONE,
    META_SCORE,
    POPULARITY,
    HEALTHINESS,
    PRICE,
    TIME,
    RANDOM,
    MAX_USED_INGREDIENTS,
    MIN_MISSING_INGREDIENTS,
    ALCOHOL,
    CAFFEINE,
    COPPER,
    ENERGY,
    CALORIES,
    CALCIUM,
    CARBOHYDRATES,
    CARBS,
    CHOLINE,
    CHOLESTEROL,
    TOTAL_FAT,
    FLUORIDE,
    TRANS_FAT,
    SATURATED_FAT,
    MONO_UNSATURATED_FAT,
    POLY_UNSATURATED_FAT,
    FIBER,
    FOLATE,
    FOLIC_ACID,
    IODINE,
    IRON,
    MAGNESIUM,
    MANGANESE,
    VITAMIN_B3,
    NIACIN,
    VITAMIN_B5,
    PANTOTHENIC_ACID,
    PHOSPHORUS,
    POTASSIUM,
    PROTEIN,
    VITAMIN_B2,
    RIBOFLAVIN,
    SELENIUM,
    SODIUM,
    VITAMIN_B1,
    THIAMIN,
    VITAMIN_A,
    VITAMIN_B6,
    VITAMIN_B12,
    VITAMIN_C,
    VITAMIN_D,
    VITAMIN_E,
    VITAMIN_K,
    SUGAR,
    ZINC,
}
