package com.vojkan.chefapp.recipedata;

import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Equipment {

    private Integer id;
    private String name;
    private String imageFile;

    public Equipment(JSONObject equipmentJSON) {
        try {
            if (equipmentJSON.has("id")) {
                this.id = (Integer) JSONNullChecker.checkForNullValueField(equipmentJSON, "id");
            }
            this.name = String.valueOf(JSONNullChecker.checkForNullValueField(equipmentJSON,"name"));
            this.imageFile = String.valueOf(JSONNullChecker.checkForNullValueField(equipmentJSON,"image"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
