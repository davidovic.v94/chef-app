package com.vojkan.chefapp.recipedata;

import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Nutrient {

    private String title;
    private Double amount;
    private String unit;
    private Double percentOfDailyNeeds;

    public Nutrient(JSONObject nutrientJSON) {
        this.title = (String) JSONNullChecker.checkForNullValueField(nutrientJSON, "title");
        this.amount = (Double) JSONNullChecker.checkForNullValueField(nutrientJSON, "amount");
        this.unit = (String) JSONNullChecker.checkForNullValueField(nutrientJSON, "unit");
        this.percentOfDailyNeeds = (Double) JSONNullChecker.checkForNullValueField(nutrientJSON, "percentOfDailyNeeds");
    }

}
