package com.vojkan.chefapp.recipedata.enums;

public enum SortOrder {
    NONE,
    ASCENDING,
    DESCENDING
}
