package com.vojkan.chefapp.recipedata;

import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Ingredient {

    private Integer id;
    private String name;
    private Double amount;
    private String unit;
    private String imageFile;
    private Double price;
    private boolean checked;

    public Ingredient(JSONObject ingredientJSON) {
        try {
            this.id = (Integer) JSONNullChecker.checkForNullValueField(ingredientJSON, "id");
            this.name = String.valueOf(JSONNullChecker.checkForNullValueField(ingredientJSON,"name"));
            this.amount = (Double) JSONNullChecker.checkForNullValueField(ingredientJSON,"amount");
            this.unit = String.valueOf(JSONNullChecker.checkForNullValueField(ingredientJSON,"unit"));
            this.imageFile = String.valueOf(JSONNullChecker.checkForNullValueField(ingredientJSON,"image"));
            this.price = (Double) JSONNullChecker.checkForNullValueField(ingredientJSON,"price");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Ingredient(String name, Double amount, String unit, Double price) {
        this.name = name;
        this.amount = amount;
        this.unit = unit;
        this.price = price;
    }

    public Ingredient(String name, String imageFile) {
        this.name = name;
        this.imageFile = imageFile;
    }

}
