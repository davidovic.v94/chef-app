package com.vojkan.chefapp.recipedata;

import java.util.List;

import lombok.Data;

@Data
public class Filter {

    private String title;
    private List<Object> list;
    private boolean expanded;

    public Filter(String title, List<Object> list) {
        this.title = title;
        this.list = list;
    }

}
