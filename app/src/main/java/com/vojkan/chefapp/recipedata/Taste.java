package com.vojkan.chefapp.recipedata;

import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Taste {

    private Double sweetness;
    private Double saltiness;
    private Double sourness;
    private Double bitterness;
    private Double savoriness;
    private Double fattiness;
    private Double spiciness;

    public Taste(JSONObject tasteJSON) {
        this.sweetness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "sweetness");
        this.saltiness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "saltiness");
        this.sourness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "sourness");
        this.bitterness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "bitterness");
        this.savoriness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "savoriness");
        this.fattiness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "fattiness");
        this.spiciness = (Double) JSONNullChecker.checkForNullValueField(tasteJSON, "spiciness");
    }

}
