package com.vojkan.chefapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.vojkan.chefapp.R;

import org.jetbrains.annotations.NotNull;

public class ProgressDialogFragment extends DialogFragment {

    private final Activity activity;

    public ProgressDialogFragment(Activity activity) {
        this.activity = activity;
    }

    public static ProgressDialogFragment newInstance(Activity activity) {
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment(activity);
        progressDialogFragment.setCancelable(true);
        return progressDialogFragment;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(activity);
        materialAlertDialogBuilder.setView(activity.getLayoutInflater().inflate(R.layout.loading_dialog, null));
        materialAlertDialogBuilder.setBackground(new ColorDrawable(66000000));

        return materialAlertDialogBuilder.create();
    }

}
