package com.vojkan.chefapp.utils;

import org.apache.commons.lang3.StringUtils;

public class StringFormatter {

    public static String capitalizeFirstLetter(String string) {
        return StringUtils.capitalize(string.toLowerCase().replace("_", " "));
    }

    public static String formatStringForRequest(String string) {
        return string.toLowerCase().replace(" ", "-");
    }

    public static String capitalizeEveryWord(String string) {
        StringBuilder retVal = new StringBuilder();
        String[] words = string.split("_");
        for (String s : words) {
            retVal.append(capitalizeFirstLetter(s)).append(" ");
        }
        retVal.deleteCharAt(retVal.length()-1);
        return retVal.toString();
    }

    public static String clearWhiteSpaces(String string) {
        return string.replace(" ", "");
    }

}
