package com.vojkan.chefapp.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.ui.ingredientsdialog.IngredientsDialog;
import com.vojkan.chefapp.ui.recipedialog.RecipeFragment;
import com.vojkan.chefapp.ui.searchresult.SearchResult;

public class CustomAnimations {

    private final Activity activity;

    public CustomAnimations(Activity activity) {
        this.activity = activity;
    }

    public void showSplitUpAnimation(View view, int recipeId) {
        AnimationSet as = new AnimationSet(true);
        as.setFillEnabled(true);
        as.setFillAfter(true);
        as.setInterpolator(new AccelerateInterpolator());

        Animation scaleFromInvisibleToDotAnim = new ScaleAnimation(0.1f, 1f, 0.1f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleFromInvisibleToDotAnim.setFillAfter(true);
        scaleFromInvisibleToDotAnim.setDuration(200);
        as.addAnimation(scaleFromInvisibleToDotAnim);

        Animation scaleFromDotToLineAnim = new ScaleAnimation(1f, 500f, 1f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleFromDotToLineAnim.setFillAfter(true);
        scaleFromDotToLineAnim.setDuration(200);
        scaleFromDotToLineAnim.setStartOffset(200);
        as.addAnimation(scaleFromDotToLineAnim);

        Animation scaleLineDotToFullScreenAnim = new ScaleAnimation(1f, 1f, 1f, 1000f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleLineDotToFullScreenAnim.setFillAfter(true);
        scaleLineDotToFullScreenAnim.setDuration(600);
        scaleLineDotToFullScreenAnim.setStartOffset(400);

        as.addAnimation(scaleLineDotToFullScreenAnim);
        as.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
                if (activity != null) {
                    ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();
                    Toolbar toolbar = ((MainActivity) activity).findViewById(R.id.toolbar);
                    MainActivity.hideActionBar(actionBar, toolbar, 300);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                DialogFragment dialogFragment = RecipeFragment.newInstance(recipeId);
                if (activity != null) {
                    dialogFragment.show(((MainActivity) activity).getSupportFragmentManager(), "Recipe_Dialog");
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.setAnimation(as);
        view.startAnimation(as);
        view.postOnAnimationDelayed(() -> {
            view.clearAnimation();
            view.setVisibility(View.INVISIBLE);
        }, 2000);
    }

    public void hideSplitUpAnimation(DialogFragment dialogFragment, View view, LinearLayout linearLayout) {
        AnimationSet as = new AnimationSet(true);
        as.setFillEnabled(true);
        as.setFillAfter(true);
        as.setInterpolator(new DecelerateInterpolator());

        Animation scaleFromFullScreenToLineAnim = new ScaleAnimation(1f, 1f, 1f, 0.001f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleFromFullScreenToLineAnim.setFillAfter(true);
        scaleFromFullScreenToLineAnim.setStartOffset(400);
        scaleFromFullScreenToLineAnim.setDuration(600);
        as.addAnimation(scaleFromFullScreenToLineAnim);

        Animation scaleFromLineToDotAnim = new ScaleAnimation(1f, 0.008f, 1f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleFromLineToDotAnim.setFillAfter(true);
        scaleFromLineToDotAnim.setDuration(200);
        scaleFromLineToDotAnim.setStartOffset(1000);
        as.addAnimation(scaleFromLineToDotAnim);

        Animation scaleFromDotToInvisibleAnim = new ScaleAnimation(1f, 1f, 1f, 0.1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleFromDotToInvisibleAnim.setFillAfter(true);
        scaleFromDotToInvisibleAnim.setDuration(200);
        scaleFromDotToInvisibleAnim.setStartOffset(1200);

        as.addAnimation(scaleFromDotToInvisibleAnim);
        as.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                linearLayout.setVisibility(View.INVISIBLE);
                if (view.getVisibility() == View.INVISIBLE) {
                    view.setVisibility(View.VISIBLE);
                }
                if (activity != null) {
                    if (MainActivity.getLastFragment() == null) {
                        ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();
                        Toolbar toolbar = ((MainActivity) activity).findViewById(R.id.toolbar);
                        if (actionBar != null) {
                            MainActivity.showActionBar(actionBar, toolbar, 300);
                        }
                    }
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dialogFragment.dismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.setAnimation(as);
        view.startAnimation(as);
    }

    public void slideDown(View view, int fromHeight) {
        Animation animation = new SlideAnimation(view, fromHeight, 0);
        animation.setDuration(500);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.setAnimation(animation);
        view.startAnimation(animation);
    }

    public void slideUp(View view, int toHeight) {
        Animation animation = new SlideAnimation(view, 0, toHeight);
        animation.setDuration(500);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.setAnimation(animation);
        view.startAnimation(animation);
    }

    public void circularRevealView(View view, int code, String text) {
        float density = activity.getResources().getDisplayMetrics().density;
        int width = activity.getResources().getDisplayMetrics().widthPixels;
        int height = activity.getResources().getDisplayMetrics().heightPixels;
        int navigationBarHeight = getNavigationBarHeight();
        float finalRadius = Math.max(width, height + navigationBarHeight);
        int centerX = view.getWidth() / 2;
        int centerY;

        if (code == 1) {
            // height from toolbar to add button center
            centerY = (int) (73 * density);
        } else {
            // height from toolbar to search button center
            centerY = (int) (578 * density);
        }

        Animator circularReveal = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, 0, finalRadius * 1.1f);
        circularReveal.setDuration(1000);
        circularReveal.setInterpolator(new AccelerateInterpolator());
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(View.VISIBLE);
                ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();
                Toolbar toolbar = ((MainActivity) activity).findViewById(R.id.toolbar);
                MainActivity.hideActionBar(actionBar, toolbar, 300);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (code == 1) {
                    DialogFragment dialogFragment = new IngredientsDialog();
                    dialogFragment.show(((MainActivity) activity).getSupportFragmentManager(), "Ingredient_Dialog");
                } else {
                    SearchResult searchResult = SearchResult.newInstance(text);
                    MainActivity.loadFragment(searchResult, "Search_Result");
                }
            }
        });
        circularReveal.start();
        view.postOnAnimationDelayed(() -> {
            view.clearAnimation();
            view.setVisibility(View.INVISIBLE);
        }, 2000);
    }

    public void circularHideView(DialogFragment dialogFragment, View view, int code) {
        float density = activity.getResources().getDisplayMetrics().density;
        int width = activity.getResources().getDisplayMetrics().widthPixels;
        int height = activity.getResources().getDisplayMetrics().heightPixels;

        float finalRadius = Math.max(width, height);
        int centerX = view.getWidth() / 2;
        int centerY;

        if (code == 1) {
            // height from toolbar to add button center
            centerY = (int) (73 * density) + 147;
        } else {
            // height from toolbar to search button center
            centerY = (int) (578 * density) + 147;
        }

        Animator circularReveal = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, finalRadius * 1.1f, 0);
        circularReveal.setDuration(1000);
        circularReveal.setInterpolator(new DecelerateInterpolator());
        circularReveal.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                view.setVisibility(View.VISIBLE);
                ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();
                Toolbar toolbar = ((MainActivity) activity).findViewById(R.id.toolbar);
                if (actionBar != null) {
                    MainActivity.showActionBar(actionBar, toolbar, 300);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (code == 1) {
                    dialogFragment.dismiss();
                } else {
                    MainActivity.removeFragment("Search_Result");
                }
            }
        });

        circularReveal.start();
    }

    private int getNavigationBarHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        if (activity != null) {
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        }
        int usableHeight = metrics.heightPixels;
        if (activity != null) {
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        }
        int realHeight = metrics.heightPixels;

        if (realHeight > usableHeight)
            return realHeight - usableHeight;
        else
            return 0;
    }

}
