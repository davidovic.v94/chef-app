package com.vojkan.chefapp.ui.ingredientsdialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputLayout;
import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.IngredientCheckboxListAdapter;
import com.vojkan.chefapp.data.DataManage;
import com.vojkan.chefapp.recipedata.Ingredient;
import com.vojkan.chefapp.ui.whatsinyourfridge.WhatsInYourFridgeFragment;
import com.vojkan.chefapp.utils.CustomAnimations;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IngredientsDialog extends DialogFragment {

    private CustomAnimations customAnimations;
    private ChipGroup chipGroup;
    private IngredientCheckboxListAdapter ingredientCheckboxListAdapter;
    private RelativeLayout dialogLayout;

    private final List<Ingredient> ingredients = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_ChefApp_NoActionBar_FullScreenDialog);
    }

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        IngredientsDialogViewModel ingredientsDialogViewModel = new ViewModelProvider(this).get(IngredientsDialogViewModel.class);
        View root = inflater.inflate(R.layout.add_ingredient_dialog, container, false);

        dialogLayout = root.findViewById(R.id.ingredient_dialog_layout);
        TextInputLayout textInputLayout = root.findViewById(R.id.ingredients_search_edit_text);
        MaterialCardView materialCardView = root.findViewById(R.id.selected_ingredients_holder);
        chipGroup = materialCardView.findViewById(R.id.ingredients_chip_group);
        MaterialButton addButton = root.findViewById(R.id.ingredients_add_button);
        RecyclerView recyclerView = root.findViewById(R.id.ingredients_recycler_view);

        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (MainActivity.ingredients.size() == 0) {
            ingredientsDialogViewModel.loadIngredients();
            DataManage dataManage = new DataManage(MainActivity.sharedPreferences);
            dataManage.loadData();
        }

        ingredients.addAll(MainActivity.ingredients);

        if (WhatsInYourFridgeFragment.ingredients != null) {
            if (WhatsInYourFridgeFragment.ingredients.size() > 0) {
                ingredients.removeAll(WhatsInYourFridgeFragment.ingredients);
            }
        }

        ingredientCheckboxListAdapter = new IngredientCheckboxListAdapter(getContext(), materialCardView, ingredients, addButton);
        recyclerView.setAdapter(ingredientCheckboxListAdapter);

        ingredientsDialogViewModel.getErrorOccurred().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });

        if (textInputLayout.getEditText() != null) {
            textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    ingredientCheckboxListAdapter.getFilter().filter(charSequence);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        materialCardView.setOnClickListener(view -> {
            if (chipGroup.getVisibility() == View.VISIBLE) {
                chipGroup.setVisibility(View.GONE);
            } else {
                chipGroup.setVisibility(View.VISIBLE);
            }
        });

        addButton.setOnClickListener(view -> {
            WhatsInYourFridgeFragment.ingredients.addAll(ingredientCheckboxListAdapter.getSelectedIngredients());
            WhatsInYourFridgeFragment.ingredientListAdapter.notifyDataSetChanged();
            customAnimations.circularHideView(this, dialogLayout, 1);
        });

        root.setFocusableInTouchMode(true);
        root.requestFocus();
        root.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                customAnimations.circularHideView(this, dialogLayout, 1);
                return true;
            }
            return false;
        });

        root.setOnTouchListener((view, motionEvent) -> {
            hideKeyboard(requireContext(), root);
            return false;
        });

        root.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            root.getWindowVisibleDisplayFrame(r);

            int heightDiff = root.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 200) {
                if (!Objects.requireNonNull(textInputLayout.getEditText()).isFocused()) {
                    textInputLayout.getEditText().requestFocus();
                }
            } else {
                if (Objects.requireNonNull(textInputLayout.getEditText()).isFocused()) {
                    textInputLayout.getEditText().clearFocus();
                }
                root.requestFocus();
            }
        });

        return root;
    }

    private void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
