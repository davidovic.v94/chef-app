package com.vojkan.chefapp.ui.findrecipes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.RecipesListAdapter;
import com.vojkan.chefapp.adapters.viewholders.CheckboxListViewHolder;
import com.vojkan.chefapp.adapters.viewholders.ChipListViewHolder;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.utils.CustomAnimations;
import com.vojkan.chefapp.utils.ProgressDialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FindRecipesFragment extends Fragment {

    private CustomAnimations customAnimations;
    private ScrollView drawerRight;
    private FindRecipesViewModel findRecipesViewModel;
    private MaterialButton searchButton, filterButton, filterSearchButton;
    private TextInputLayout textInputLayout;
    private RecipesListAdapter recipesListAdapter;
    private View animationView;
    public ProgressDialogFragment progressDialogFragment;
    private MenuItem search, filter;

    private List<Recipe> recipes = new ArrayList<>();

    private final int SEARCH_INPUT_LAYOUT_HEIGHT_PX = 161;
    private final int BUTTON_HEIGHT_PX = 131;
    private final StringBuilder allParameters = new StringBuilder();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        findRecipesViewModel = new ViewModelProvider(this).get(FindRecipesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_find_recipes, container, false);

        progressDialogFragment = ProgressDialogFragment.newInstance(getActivity());
        textInputLayout = root.findViewById(R.id.find_recipes_search_edit_text);
        RecyclerView recyclerView = root.findViewById(R.id.recycler_view);
        searchButton = root.findViewById(R.id.find_recipes_search_button);
        filterButton = root.findViewById(R.id.find_recipes_filter_button);
        animationView = root.findViewById(R.id.expand_animation_view);

        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
            filterSearchButton = getActivity().findViewById(R.id.filter_search_button);
            drawerRight = getActivity().findViewById(R.id.drawer_right);
        }

        recipes = findRecipesViewModel.getRecipes();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recipesListAdapter = new RecipesListAdapter(getActivity(), recyclerView, recipes);
        recyclerView.setAdapter(recipesListAdapter);
        recipesListAdapter.setOnItemClickListener(onItemClickListener);
        recipesListAdapter.setLoadMore(() -> {
            if (recipes.size() < findRecipesViewModel.getTotalResults()) {
                recipes.add(null);
                recipesListAdapter.notifyItemInserted(recipes.size() - 1);
                new Handler().postDelayed(() -> {
                    recipes.remove(recipes.size() - 1);
                    recipesListAdapter.notifyItemRemoved(recipes.size());

                    findRecipesViewModel.search(allParameters.toString());
                    recipesListAdapter.setLoaded();
                }, 5000);
            } else {
                Toast.makeText(getActivity(), "There is no more items!", Toast.LENGTH_SHORT).show();
            }
        });

        findRecipesViewModel.getLoadedMutableLiveData().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                progressDialogFragment.dismiss();
                recipesListAdapter.notifyDataSetChanged();
            }
        });

        findRecipesViewModel.getErrorOccurred().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });

        findRecipesViewModel.getNoResult().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                Toast.makeText(getContext(), "There are no results for specific search!", Toast.LENGTH_SHORT).show();
            }
        });

        searchButton.setOnClickListener(searchButtonOnClickListener);
        filterSearchButton.setOnClickListener(searchButtonOnClickListener);

        if (getActivity() != null) {
            filterButton.setOnClickListener(((MainActivity) getActivity()).filterButtonOnClickListener);
        }

        root.setFocusableInTouchMode(true);
        root.setOnTouchListener((view, motionEvent) -> {
            hideKeyboard(requireContext(), root);
            return false;
        });

        root.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            root.getWindowVisibleDisplayFrame(r);

            int heightDiff = root.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 200) {
                if (!Objects.requireNonNull(textInputLayout.getEditText()).isFocused()) {
                    textInputLayout.getEditText().requestFocus();
                }
            } else {
                if (Objects.requireNonNull(textInputLayout.getEditText()).isFocused()) {
                    textInputLayout.getEditText().clearFocus();
                }
                root.requestFocus();
            }
        });

        return root;
    }

    RecipesListAdapter.OnItemClickListener onItemClickListener = position -> {
        if (getActivity() != null) {
            setMargins(animationView);
            customAnimations.showSplitUpAnimation(animationView, recipes.get(position).getId());
        }
    };

    View.OnClickListener searchButtonOnClickListener = view -> {
        recipes.clear();
        setAllParameters();

        findRecipesViewModel.search(allParameters.toString());

        if (getActivity() != null) {
            progressDialogFragment.show(getActivity().getSupportFragmentManager(), "Progress_Dialog");
        }

        customAnimations.slideDown(searchButton, BUTTON_HEIGHT_PX);
        customAnimations.slideDown(textInputLayout, SEARCH_INPUT_LAYOUT_HEIGHT_PX);
        customAnimations.slideUp(filterButton, BUTTON_HEIGHT_PX);
        hideKeyboard(requireContext(), this.requireView());

        if (!search.isVisible()) {
            search.setVisible(true);
        }

        if (filter.isVisible()) {
            filter.setVisible(false);
        }

        if (MainActivity.drawerLayout.isDrawerOpen(drawerRight)) {
            MainActivity.drawerLayout.closeDrawer(drawerRight);
        }
    };

    private void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        search = menu.findItem(R.id.action_search);
        filter = menu.findItem(R.id.action_filter);
        search.setOnMenuItemClickListener(menuItem -> {
            if (textInputLayout.getVisibility() == View.GONE) {
                customAnimations.slideDown(filterButton, BUTTON_HEIGHT_PX);
                customAnimations.slideUp(textInputLayout, SEARCH_INPUT_LAYOUT_HEIGHT_PX);
                customAnimations.slideUp(searchButton, BUTTON_HEIGHT_PX);
                search.setVisible(false);
                filter.setVisible(true);
                return true;
            }
            hideKeyboard(requireContext(), this.requireView());
            return false;
        });
        if (!filter.isVisible()) {
            filter.setVisible(true);
        }
        filter.setOnMenuItemClickListener(menuItem -> {
            if (filterButton.getVisibility() == View.GONE) {
                customAnimations.slideDown(textInputLayout, SEARCH_INPUT_LAYOUT_HEIGHT_PX);
                customAnimations.slideDown(searchButton, BUTTON_HEIGHT_PX);
                customAnimations.slideUp(filterButton, BUTTON_HEIGHT_PX);
                filter.setVisible(false);
                search.setVisible(true);
                hideKeyboard(requireContext(), this.requireView());
                return true;
            }
            return false;
        });
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        hideKeyboard(requireContext(), this.requireView());
        return super.onOptionsItemSelected(item);
    }

    public void setMargins(View v) {
        int marginTop = MainActivity.marginTop;
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(0, marginTop, 0, 0);
            v.requestLayout();
        }
    }

    private void setAllParameters() {
        if (allParameters.length() != 0) {
            allParameters.delete(0, allParameters.length());
        }

        if (Objects.requireNonNull(textInputLayout.getEditText()).getText().toString().length() != 0) {
            allParameters.append("&query=");
            allParameters.append(textInputLayout.getEditText().getText().toString());
        }
        setCuisineParameter();
        setIntolerancesParameter();
        setDietParameter();
        setTypeParameter();

        if (MainActivity.nutrition.length() != 0) {
            allParameters.append(MainActivity.nutrition.toString());
        }
        allParameters.append("&instructionsRequired=");
        allParameters.append(MainActivity.instructionsRequired);
        allParameters.append("&addRecipeInformation=");
        allParameters.append(MainActivity.addRecipeInformation);
        allParameters.append("&addRecipeNutrition=");
        allParameters.append(MainActivity.addRecipeNutrition);

        if (MainActivity.maxReadyTime >= 5) {
            allParameters.append("&maxReadyTime=");
            allParameters.append(MainActivity.maxReadyTime);
        }
        if (!MainActivity.sortBy.equals("None")) {
            allParameters.append("&sort=");
            allParameters.append(MainActivity.sortBy);
        }
        if (!MainActivity.sortBy.equals("None")) {
            allParameters.append("&sortDirection=");
            allParameters.append(MainActivity.sortOrder);
        }
    }

    private void setCuisineParameter() {
        if (ChipListViewHolder.checkedCuisinesList.size() > 0) {
            allParameters.append("&cuisine=");
            for (String s : ChipListViewHolder.checkedCuisinesList) {
                allParameters.append(s).append(",");
            }
            allParameters.replace(allParameters.length() - 1, allParameters.length(), "");
        }
    }

    private void setIntolerancesParameter() {
        if (CheckboxListViewHolder.checkedIntolerancesList.size() > 0) {
            allParameters.append("&intolerances=");
            for (String s : CheckboxListViewHolder.checkedIntolerancesList) {
                allParameters.append(s).append(",");
            }
            allParameters.replace(allParameters.length() - 1, allParameters.length(), "");
        }
    }

    private void setDietParameter() {
        if (ChipListViewHolder.checkedDietsList.size() > 0) {
            allParameters.append("&diet=");
            for (String s : ChipListViewHolder.checkedDietsList) {
                allParameters.append(s).append(",");
            }
            allParameters.replace(allParameters.length() - 1, allParameters.length(), "");
        }
    }

    private void setTypeParameter() {
        if (CheckboxListViewHolder.checkedMealTypesList.size() > 0) {
            allParameters.append("&type=");
            for (String s : CheckboxListViewHolder.checkedMealTypesList) {
                allParameters.append(s).append(",");
            }
            allParameters.replace(allParameters.length() - 1, allParameters.length(), "");
        }
    }

}