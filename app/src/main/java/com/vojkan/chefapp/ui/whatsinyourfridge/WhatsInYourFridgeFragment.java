package com.vojkan.chefapp.ui.whatsinyourfridge;

import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.IngredientListAdapter;
import com.vojkan.chefapp.recipedata.Ingredient;
import com.vojkan.chefapp.utils.CustomAnimations;

import java.util.ArrayList;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class WhatsInYourFridgeFragment extends Fragment {

    private CustomAnimations customAnimations;
    private RecyclerView recyclerView;
    public static IngredientListAdapter ingredientListAdapter;
    public static List<Ingredient> ingredients = new ArrayList<>();
    public static List<Ingredient> ingredientsForRemoval = new ArrayList<>();
    private View circularRevealingView;
    private final StringBuilder parameterStringBuilder = new StringBuilder();

    private final int ADD_BUTTON_CODE = 1;
    private final int SEARCH_BUTTON_CODE = 2;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_whats_in_your_fridge, container, false);

        MaterialButton addIngredientButton = root.findViewById(R.id.whats_in_your_fridge_add_button);
        MaterialButton searchRecipesButton = root.findViewById(R.id.whats_in_your_fridge_search_button);
        recyclerView = root.findViewById(R.id.recycler_view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        ingredientListAdapter = new IngredientListAdapter(ingredients);
        recyclerView.setAdapter(ingredientListAdapter);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        circularRevealingView = root.findViewById(R.id.circular_reveal_view);

        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
        }

        addIngredientButton.setOnClickListener(view -> customAnimations.circularRevealView(circularRevealingView, ADD_BUTTON_CODE, null));

        searchRecipesButton.setOnClickListener(view -> {
            if (ingredients.size() == 0) {
                Toast.makeText(getContext(), "There is no selected ingredients!", Toast.LENGTH_SHORT).show();
                return;
            }
            getSearchParameters();
            customAnimations.circularRevealView(circularRevealingView, SEARCH_BUTTON_CODE, parameterStringBuilder.toString());
        });

        return root;
    }

    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            return super.getMovementFlags(recyclerView, viewHolder);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            if (direction == ItemTouchHelper.LEFT) {
                Ingredient forDeletion = ingredients.get(position);
                ingredientsForRemoval.add(forDeletion);
                ingredients.remove(position);
                ingredientListAdapter.notifyItemRemoved(position);
                Snackbar snackbar = Snackbar.make(recyclerView, String.format("Deleting %s...", forDeletion.getName()), Snackbar.LENGTH_LONG);
                snackbar.setAction("Undo", view -> {
                    ingredients.add(position, forDeletion);
                    ingredientListAdapter.notifyItemInserted(position);
                    ingredientsForRemoval.remove(forDeletion);
                });
                snackbar.show();
            }
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addSwipeLeftBackgroundColor(ContextCompat.getColor(requireContext(), R.color.md_red_400))
                    .addSwipeLeftLabel("Delete")
                    .addSwipeLeftActionIcon(R.drawable.ic_baseline_delete_24)
                    .create()
                    .decorate();

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }

    });

    private void getSearchParameters() {
        if (parameterStringBuilder.length() > 0) {
            parameterStringBuilder.delete(0, parameterStringBuilder.length());
        }
        for (Ingredient ingredient : ingredients) {
            parameterStringBuilder.append(ingredient.getName()).append(",+");
        }
        parameterStringBuilder.replace(parameterStringBuilder.length() - 1, parameterStringBuilder.length(), "");
    }

}