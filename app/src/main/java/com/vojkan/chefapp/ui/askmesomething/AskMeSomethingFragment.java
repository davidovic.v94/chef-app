package com.vojkan.chefapp.ui.askmesomething;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.vojkan.chefapp.R;

public class AskMeSomethingFragment extends Fragment {

    private AskMeSomethingViewModel askMeSomethingViewModel;
    private TextInputLayout textInputLayout;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        askMeSomethingViewModel =
                new ViewModelProvider(this).get(AskMeSomethingViewModel.class);
        View root = inflater.inflate(R.layout.fragment_ask_me_something, container, false);
        TextView title = root.findViewById(R.id.text_ask_me_something);
        TextView answer = root.findViewById(R.id.ask_me_something_answer_text);
        textInputLayout = root.findViewById(R.id.ask_me_something_edit_text);
        MaterialButton submitButton = root.findViewById(R.id.ask_me_something_button);

        askMeSomethingViewModel.getTitle().observe(getViewLifecycleOwner(), title::setText);
        askMeSomethingViewModel.getAnswer().observe(getViewLifecycleOwner(), answer::setText);

        submitButton.setOnClickListener(view -> {
            if (textInputLayout.getEditText() != null) {
                if (textInputLayout.getEditText().getText().length() > 0) {
                    askMeSomethingViewModel.getAnswer(formatQuestion(textInputLayout.getEditText().getText().toString()));
                } else {
                    Toast.makeText(getContext(), "Ask me something", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return root;
    }

    private String formatQuestion(String question) {
        return question.replace(" ", "+");
    }

}