package com.vojkan.chefapp.ui.askmesomething;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vojkan.chefapp.network.BaseTask;
import com.vojkan.chefapp.network.TaskRunner;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import lombok.Data;

@Data
public class AskMeSomethingViewModel extends ViewModel {

    private TaskRunner taskRunner = new TaskRunner();
    private MutableLiveData<String> title, answer;

    public AskMeSomethingViewModel() {
        title = new MutableLiveData<>();
        title.setValue("Ask a question");
        answer = new MutableLiveData<>();
    }

    public void getAnswer(String question) {
        final String URL = "https://api.spoonacular.com/recipes/quickAnswer?q=" + question + "&apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new AskMeSomethingViewModel.Task(URL));
    }

    private class Task extends BaseTask<String> {

        final String stringUrl;

        public Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int dataStreamReader = inputStreamReader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataStreamReader != -1) {
                    char current = (char) dataStreamReader;
                    stringBuilder.append(current);
                    dataStreamReader = inputStreamReader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.has("answer")) {
                    answer.setValue(jsonObject.getString("answer"));
                } else {
                    answer.setValue("I have no answer for that question.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                answer.setValue("Something went wrong!");
            }
        }
    }

}