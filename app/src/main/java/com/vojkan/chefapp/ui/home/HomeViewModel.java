package com.vojkan.chefapp.ui.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vojkan.chefapp.network.BaseTask;
import com.vojkan.chefapp.network.TaskRunner;
import com.vojkan.chefapp.recipedata.Recipe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class HomeViewModel extends ViewModel {

    private TaskRunner taskRunner = new TaskRunner();
    private List<Recipe> recipes = new ArrayList<>();
    private JSONArray recipesJSON;
    private MutableLiveData<String> title, header;
    private MutableLiveData<Boolean> loadedMutableLiveData, errorOccurred;
    private int offset = 0, totalResults = 50;
    private int random;

    public HomeViewModel() {
        header = new MutableLiveData<>();
        title = new MutableLiveData<>();
        loadedMutableLiveData = new MutableLiveData<>();
        errorOccurred = new MutableLiveData<>();
        loadRecipes(totalResults);
    }

    public void loadMoreRecipes() {
        parseJSONResponse(recipesJSON);
    }

    private void loadRecipes(int number) {
//        if (MainActivity.favoriteRecipes.size() != 0) {
//            random = new Random().nextInt(MainActivity.favoriteRecipes.size());
//            final String URL = "https://api.spoonacular.com/recipes/" + MainActivity.favoriteRecipes.get(random).getId() + "/similar?number=" + number + "&apiKey=916e26022c804c5a9957198f0e7cdf9e";
//            taskRunner.executeAsync(new Task(URL));
//        } else {
            final String URL = "https://api.spoonacular.com/recipes/random?number=" + number + "&apiKey=916e26022c804c5a9957198f0e7cdf9e";
            taskRunner.executeAsync(new Task(URL));
//        }
    }

    private class Task extends BaseTask<String> {

        final String stringUrl;

        public Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int dataStreamReader = inputStreamReader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataStreamReader != -1) {
                    char current = (char) dataStreamReader;
                    stringBuilder.append(current);
                    dataStreamReader = inputStreamReader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                JSONObject response = new JSONObject(result);
                recipesJSON = response.getJSONArray("recipes");
                parseJSONResponse(recipesJSON);
            } catch (Exception e) {
                e.printStackTrace();
                errorOccurred.setValue(true);
            } finally {
                loadedMutableLiveData.setValue(true);
            }
        }
    }

    private void parseJSONResponse(JSONArray recipesJSON) {
        try {
            JSONObject recipeJSON;
            int a = getOffset() * 10;
            int b = getOffset() * 10 + 10;
            for (int i = a; i < b; i++) {
                recipeJSON = recipesJSON.getJSONObject(i);
                Recipe recipe = new Recipe();
                recipe.setId(recipeJSON.getInt("id"));
                recipe.setName(recipeJSON.getString("title"));
                if (recipeJSON.has("image")) {
                    recipe.setImageUrl(recipeJSON.getString("image"));
                }
                if (recipeJSON.has("spoonacularSourceUrl")) {
                    recipe.setRecipeUrl(recipeJSON.getString("spoonacularSourceUrl"));
                }
                recipes.add(recipe);
            }
            offset++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}