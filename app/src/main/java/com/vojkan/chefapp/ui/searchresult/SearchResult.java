package com.vojkan.chefapp.ui.searchresult;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.RecipesListAdapter;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.recipedata.enums.MealType;
import com.vojkan.chefapp.utils.CustomAnimations;
import com.vojkan.chefapp.utils.ProgressDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class SearchResult extends Fragment {

    private CustomAnimations customAnimations;
    private SearchResultViewModel searchResultViewModel;
    private RelativeLayout dialogLayout;
    private RecipesListAdapter recipesListAdapter;
    private String ingredients, mealType;
    public ProgressDialogFragment progressDialogFragment;

    private List<Recipe> recipes = new ArrayList<>();
    private View animateViewFirst;

    public static SearchResult newInstance(String searchParameters) {
        Bundle args = new Bundle();
        args.putString("searchParameters", searchParameters);

        SearchResult fragment = new SearchResult();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            for (MealType mealType : MealType.values()) {
                if (getArguments().getString("searchParameters").toLowerCase().equals(mealType.name().toLowerCase().replace("_", " "))) {
                    this.mealType = getArguments().getString("searchParameters");
                    break;
                }
            }
            if (mealType == null) {
                ingredients = getArguments().getString("searchParameters");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        searchResultViewModel = new ViewModelProvider(this).get(SearchResultViewModel.class);
        View root = inflater.inflate(R.layout.search_result_dialog, container, false);

        animateViewFirst = root.findViewById(R.id.animate_view_first);
        dialogLayout = root.findViewById(R.id.search_result_dialog_layout);
        RecyclerView recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        progressDialogFragment = ProgressDialogFragment.newInstance(getActivity());
        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
            progressDialogFragment.show(getActivity().getSupportFragmentManager(), "Progress_Dialog");
        }

        recipes = searchResultViewModel.getRecipes();

        recipesListAdapter = new RecipesListAdapter(getActivity(), recyclerView, recipes);
        recyclerView.setAdapter(recipesListAdapter);
        recipesListAdapter.setOnItemClickListener(onItemClickListener);

        searchResultViewModel.getLoadedMutableLiveData().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                progressDialogFragment.dismiss();
                recipesListAdapter.notifyDataSetChanged();
            }
        });

        searchResultViewModel.getErrorOccurred().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });

        recipesListAdapter.setLoadMore(() -> {
            if (recipes.size() < searchResultViewModel.getTotalResults()) {
                recipes.add(null);
                recipesListAdapter.notifyItemInserted(recipes.size() - 1);
                new Handler().postDelayed(() -> {
                    recipes.remove(recipes.size() - 1);
                    recipesListAdapter.notifyItemRemoved(recipes.size());

                    if (ingredients != null) {
                        searchResultViewModel.loadMoreRecipes();
                        recipesListAdapter.notifyDataSetChanged();
                    }

                    if (mealType != null) {
                        searchResultViewModel.search(mealType);
                    }

                    recipesListAdapter.setLoaded();
                }, 5000);
            } else {
                Toast.makeText(getActivity(), "There is no more items!", Toast.LENGTH_SHORT).show();
            }
        });

        if (ingredients != null) {
            searchResultViewModel.loadRecipesByIngredients(ingredients);
        }

        if (mealType != null) {
            searchResultViewModel.search(mealType);
        }

        root.setFocusableInTouchMode(true);
        root.requestFocus();
        root.setOnKeyListener((v, keyCode, event) -> {
            if( keyCode == KeyEvent.KEYCODE_BACK )
            {
                customAnimations.circularHideView(null, dialogLayout, 2);
                return true;
            }
            return false;
        });

        return root;
    }

    RecipesListAdapter.OnItemClickListener onItemClickListener = position -> {
        if (getActivity() != null) {
            setMargins(animateViewFirst);
            customAnimations.showSplitUpAnimation(animateViewFirst, recipes.get(position).getId());
        }
    };

    public void setMargins(View v) {
        int marginTop = MainActivity.marginTop;
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(0, marginTop, 0, 0);
            v.requestLayout();
        }
    }

}
