package com.vojkan.chefapp.ui.home;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.RecipesListAdapter;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.utils.CustomAnimations;
import com.vojkan.chefapp.utils.ProgressDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private CustomAnimations customAnimations;
    private HomeViewModel homeViewModel;
    private RecipesListAdapter recipesListAdapter;
    public ProgressDialogFragment progressDialogFragment;

    private View animationView;

    private List<Recipe> recipes = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        animationView = root.findViewById(R.id.expand_animation_view);
        RecyclerView recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        progressDialogFragment = ProgressDialogFragment.newInstance(getActivity());

        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
            progressDialogFragment.show(getActivity().getSupportFragmentManager(), "Progress_Dialog");
        }

        recipes = homeViewModel.getRecipes();

        recipesListAdapter = new RecipesListAdapter(getActivity(), recyclerView, recipes);
        recyclerView.setAdapter(recipesListAdapter);
        recipesListAdapter.setOnItemClickListener(onItemClickListener);

        homeViewModel.getLoadedMutableLiveData().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                progressDialogFragment.dismiss();
                recipesListAdapter.notifyDataSetChanged();
            }
        });

        homeViewModel.getErrorOccurred().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });

        recipesListAdapter.setLoadMore(() -> {
            if (recipes.size() < homeViewModel.getTotalResults()) {
                recipes.add(null);
                recipesListAdapter.notifyItemInserted(recipes.size() - 1);
                new Handler().postDelayed(() -> {
                    recipes.remove(recipes.size() - 1);
                    recipesListAdapter.notifyItemRemoved(recipes.size());

                    homeViewModel.loadMoreRecipes();
                    recipesListAdapter.notifyDataSetChanged();
                    recipesListAdapter.setLoaded();
                }, 5000);
            } else {
                Toast.makeText(getActivity(), "There is no more items!", Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }

    RecipesListAdapter.OnItemClickListener onItemClickListener = position -> {
        if (getActivity() != null) {
            setMargins(animationView);
            customAnimations.showSplitUpAnimation(animationView, recipes.get(position).getId());
        }
    };

    public void setMargins(View v) {
        int marginTop = MainActivity.marginTop;
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(0, marginTop, 0, 0);
            v.requestLayout();
        }
    }

}