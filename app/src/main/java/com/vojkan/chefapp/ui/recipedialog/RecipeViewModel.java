package com.vojkan.chefapp.ui.recipedialog;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vojkan.chefapp.network.BaseTask;
import com.vojkan.chefapp.network.TaskRunner;
import com.vojkan.chefapp.recipedata.Equipment;
import com.vojkan.chefapp.recipedata.Ingredient;
import com.vojkan.chefapp.recipedata.Instruction;
import com.vojkan.chefapp.recipedata.Nutrient;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.recipedata.Taste;
import com.vojkan.chefapp.utils.JSONNullChecker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class RecipeViewModel extends ViewModel {

    private TaskRunner taskRunner = new TaskRunner();
    private MutableLiveData<Recipe> recipeMutableLiveData;
    private MutableLiveData<Taste> tasteMutableLiveData;
    private MutableLiveData<List<Ingredient>> ingredientsMutableLiveData;
    private MutableLiveData<Double> totalCost, costPerServing;
    private MutableLiveData<Boolean> errorOccurred;
    private Recipe recipe;
    private Taste taste;
    private List<Nutrient> nutrients = new ArrayList<>();
    private List<Ingredient> ingredients = new ArrayList<>();
    private MutableLiveData<Boolean> loadedMutableLiveData;

    public RecipeViewModel() {
        recipeMutableLiveData = new MutableLiveData<>();
        tasteMutableLiveData = new MutableLiveData<>();
        ingredientsMutableLiveData = new MutableLiveData<>();
        loadedMutableLiveData = new MutableLiveData<>();
        totalCost = new MutableLiveData<>();
        costPerServing = new MutableLiveData<>();
        errorOccurred = new MutableLiveData<>();
    }

    public void loadRecipes(int id) {
        final String URL = "https://api.spoonacular.com/recipes/" + id + "/information?includeNutrition=true&apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new RecipeViewModel.Task(URL));
    }

    public void getTaste(int id) {
        final String URL = "https://api.spoonacular.com/recipes/" + id + "/tasteWidget.json?apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new RecipeViewModel.Task(URL));
    }

    public void getPriceBreakdown(int id) {
        final String URL = "https://api.spoonacular.com/recipes/" + id + "/priceBreakdownWidget.json?apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new RecipeViewModel.Task(URL));
    }

    private class Task extends BaseTask<String> {

        final String stringUrl;

        public Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int dataStreamReader = inputStreamReader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataStreamReader != -1) {
                    char current = (char) dataStreamReader;
                    stringBuilder.append(current);
                    dataStreamReader = inputStreamReader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                if (stringUrl.contains("tasteWidget")) {
                    JSONObject tasteJSON = new JSONObject(result);
                    taste = new Taste(tasteJSON);
                    tasteMutableLiveData.setValue(taste);
                } else if (stringUrl.contains("information")) {
                    parseJSONResponse(result);
                    recipeMutableLiveData.setValue(recipe);
                } else if (stringUrl.contains("priceBreakdownWidget")) {
                    getPrice(result);
                    ingredientsMutableLiveData.setValue(ingredients);
                }
            } catch (Exception e) {
                e.printStackTrace();
                errorOccurred.setValue(true);
            } finally {
                loadedMutableLiveData.setValue(true);
            }
        }
    }

    private void parseJSONResponse(String result) {
        try {
            JSONObject recipeJSON = new JSONObject(result);
            recipe = new Recipe(recipeJSON);
            recipe.setIngredients(getIngredients(recipeJSON, "extendedIngredients"));
            recipe.setAnalyzedInstructions(getInstructions(recipeJSON));
            recipe.setNutrients(getNutrients(recipeJSON));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Set<Nutrient> getNutrients(JSONObject recipeJSON) {
        try {
            Set<Nutrient> nutrients = new HashSet<>();
            if (JSONNullChecker.checkForNullValueObject(recipeJSON, "nutrition") != null) {
                JSONObject nutritionJSON = recipeJSON.getJSONObject("nutrition");
                if (JSONNullChecker.checkForNullValueArray(nutritionJSON, "nutrients") != null) {
                    JSONArray nutrientsJsonArray = nutritionJSON.getJSONArray("nutrients");
                    for (int i = 0; i < nutrientsJsonArray.length(); i++) {
                        JSONObject jsonObject = nutrientsJsonArray.getJSONObject(i);
                        Nutrient nutrient = new Nutrient(jsonObject);
                        nutrients.add(nutrient);
                    }
                }
            }
            return nutrients;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void getPrice(String result) {
        try {
            JSONObject priceJSON = new JSONObject(result);
            if (JSONNullChecker.checkForNullValueArray(priceJSON, "ingredients") != null) {
                JSONArray ingredientsJsonArray = priceJSON.getJSONArray("ingredients");
                for (int i = 0; i < ingredientsJsonArray.length(); i++) {
                    JSONObject jsonObject = ingredientsJsonArray.getJSONObject(i);
                    String name = jsonObject.getString("name");
                    String unit = jsonObject.getJSONObject("amount").getJSONObject("us").getString("unit");
                    double value = jsonObject.getJSONObject("amount").getJSONObject("us").getDouble("value");
                    double price = jsonObject.getDouble("price") / 100.0;
                    ingredients.add(new Ingredient(name, value, unit, price));
                }
            }
            totalCost.setValue(priceJSON.getDouble("totalCost") / 100.0);
            costPerServing.setValue(priceJSON.getDouble("totalCostPerServing") / 100.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Set<Instruction> getInstructions(JSONObject recipeJSON) {
        try {
            Set<Instruction> instructions = new HashSet<>();
            if (JSONNullChecker.checkForNullValueArray(recipeJSON, "analyzedInstructions") != null) {
                if (recipeJSON.getJSONArray("analyzedInstructions").length() > 0) {
                    JSONArray instructionsJSON = recipeJSON.getJSONArray("analyzedInstructions").getJSONObject(0).getJSONArray("steps");
                    JSONObject instructionJSON;
                    for (int j = 0; j < instructionsJSON.length(); j++) {
                        instructionJSON = instructionsJSON.getJSONObject(j);
                        Instruction instruction = new Instruction(instructionJSON);
                        instruction.setIngredients(getIngredients(instructionJSON, "ingredients"));
                        instruction.setEquipments(getEquipments(instructionJSON));
                        instructions.add(instruction);
                    }
                }
            }
            return instructions;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Set<Ingredient> getIngredients(JSONObject jsonObject, String key) {
        try {
            Set<Ingredient> ingredients = new HashSet<>();
            if (key.equals("extendedIngredients")) {
                if (JSONNullChecker.checkForNullValueArray(jsonObject, "extendedIngredients") != null) {
                    if (jsonObject.getJSONArray("extendedIngredients").length() > 0) {
                        JSONArray ingredientsJSON = jsonObject.getJSONArray("extendedIngredients");
                        JSONObject ingredientJSON;
                        for (int i = 0; i < ingredientsJSON.length(); i++) {
                            ingredientJSON = ingredientsJSON.getJSONObject(i);
                            Ingredient ingredient = new Ingredient(ingredientJSON);
                            ingredients.add(ingredient);
                        }
                    }
                }
            } else if (key.equals("ingredients")) {
                if (JSONNullChecker.checkForNullValueArray(jsonObject, "ingredients") != null) {
                    if (jsonObject.getJSONArray("ingredients").length() > 0) {
                        JSONArray ingredientsJSON = jsonObject.getJSONArray("ingredients");
                        JSONObject ingredientJSON;
                        for (int i = 0; i < ingredientsJSON.length(); i++) {
                            ingredientJSON = ingredientsJSON.getJSONObject(i);
                            Ingredient ingredient = new Ingredient(ingredientJSON);
                            ingredients.add(ingredient);
                        }
                    }
                }
            }
            return ingredients;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Set<Equipment> getEquipments(JSONObject jsonObject) {
        try {
            Set<Equipment> equipments = new HashSet<>();
            if (JSONNullChecker.checkForNullValueArray(jsonObject, "equipment") != null) {
                if (jsonObject.getJSONArray("equipment").length() > 0) {
                    JSONArray equipmentsJSON = jsonObject.getJSONArray("equipment");
                    JSONObject equipmentJSON;
                    for (int i = 0; i < equipmentsJSON.length(); i++) {
                        equipmentJSON = equipmentsJSON.getJSONObject(i);
                        Equipment equipment = new Equipment(equipmentJSON);
                        equipments.add(equipment);
                    }
                }
            }
            return equipments;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}