package com.vojkan.chefapp.ui.favoriterecipes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.RecipesListAdapter;
import com.vojkan.chefapp.utils.CustomAnimations;

public class FavoriteRecipesFragment extends Fragment {

    private CustomAnimations customAnimations;
    private View animationView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favorite_recipes, container, false);

        animationView = root.findViewById(R.id.expand_animation_view);
        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
        }

        RecyclerView recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        RecipesListAdapter recipesListAdapter = new RecipesListAdapter(getActivity(), recyclerView, MainActivity.favoriteRecipes);
        recyclerView.setAdapter(recipesListAdapter);
        recipesListAdapter.setOnItemClickListener(onItemClickListener);

        return root;
    }

    RecipesListAdapter.OnItemClickListener onItemClickListener = position -> {
        if (getActivity() != null) {
            setMargins(animationView);
            customAnimations.showSplitUpAnimation(animationView, MainActivity.favoriteRecipes.get(position).getId());
        }
    };

    public void setMargins(View v) {
        int marginTop = MainActivity.marginTop;
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(0, marginTop, 0, 0);
            v.requestLayout();
        }
    }

}