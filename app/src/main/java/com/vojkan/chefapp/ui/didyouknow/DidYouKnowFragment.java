package com.vojkan.chefapp.ui.didyouknow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.button.MaterialButton;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.utils.ProgressDialogFragment;

public class DidYouKnowFragment extends Fragment {

    private DidYouKnowViewModel didYouKnowViewModel;
    public ProgressDialogFragment progressDialogFragment;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        didYouKnowViewModel =
                new ViewModelProvider(this).get(DidYouKnowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_did_you_know, container, false);
        TextView randomTriviaTextView = root.findViewById(R.id.text_random_trivia);
        MaterialButton getTriviaButton = root.findViewById(R.id.get_random_trivia_button);
        progressDialogFragment = ProgressDialogFragment.newInstance(getActivity());

        didYouKnowViewModel.getTrivia().observe(getViewLifecycleOwner(), randomTriviaTextView::setText);

        if (getActivity() != null) {
            progressDialogFragment.show(getActivity().getSupportFragmentManager(), "Progress_Dialog");
        }

        didYouKnowViewModel.getRandomTrivia();

        getTriviaButton.setOnClickListener(view -> {
            if (getActivity() != null) {
                progressDialogFragment.show(getActivity().getSupportFragmentManager(), "Progress_Dialog");
            }
            didYouKnowViewModel.getRandomTrivia();
        });

        didYouKnowViewModel.getLoadedMutableLiveData().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                progressDialogFragment.dismiss();
            }
        });

        return root;
    }
}