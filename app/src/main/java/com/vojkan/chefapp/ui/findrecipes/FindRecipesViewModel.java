package com.vojkan.chefapp.ui.findrecipes;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vojkan.chefapp.network.BaseTask;
import com.vojkan.chefapp.network.TaskRunner;
import com.vojkan.chefapp.recipedata.Recipe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class FindRecipesViewModel extends ViewModel {

    private TaskRunner taskRunner = new TaskRunner();
    private List<Recipe> recipes = new ArrayList<>();
    private MutableLiveData<String> title, header;
    private MutableLiveData<Boolean> loadedMutableLiveData, errorOccurred, noResult;
    private int offset = 0, totalResults = 0;
    private String params;

    public FindRecipesViewModel() {
        header = new MutableLiveData<>();
        loadedMutableLiveData = new MutableLiveData<>();
        errorOccurred = new MutableLiveData<>();
        noResult = new MutableLiveData<>();
    }

    public void search(String parameters) {
        if (!parameters.equals(params)) {
            offset = 0;
        }
        loadRecipes(offset * 10, parameters);
        offset++;
        params = parameters;
    }

    public void loadRecipes(int offset, String parameters) {
        final String URL = "https://api.spoonacular.com/recipes/complexSearch?offset=" + offset + parameters + "&number=10&apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new FindRecipesViewModel.Task(URL));
    }

    private class Task extends BaseTask<String> {

        final String stringUrl;

        public Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int dataStreamReader = inputStreamReader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataStreamReader != -1) {
                    char current = (char) dataStreamReader;
                    stringBuilder.append(current);
                    dataStreamReader = inputStreamReader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                parseJSONResponse(result);
            } catch (Exception e) {
                e.printStackTrace();
                errorOccurred.setValue(true);
            } finally {
                loadedMutableLiveData.setValue(true);
            }
        }
    }

    private void parseJSONResponse(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray recipesJSON = response.getJSONArray("results");
            JSONObject recipeJSON;
            totalResults = response.getInt("totalResults");
            if (totalResults == 0) {
                noResult.setValue(true);
                return;
            }
            for (int i = 0; i < recipesJSON.length(); i++) {
                recipeJSON = recipesJSON.getJSONObject(i);
                Recipe recipe = new Recipe();
                recipe.setId(recipeJSON.getInt("id"));
                recipe.setName(recipeJSON.getString("title"));
                recipe.setImageUrl(recipeJSON.getString("image"));
                if (recipeJSON.has("spoonacularSourceUrl")) {
                    recipe.setRecipeUrl(recipeJSON.getString("spoonacularSourceUrl"));
                }
                recipes.add(recipe);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}