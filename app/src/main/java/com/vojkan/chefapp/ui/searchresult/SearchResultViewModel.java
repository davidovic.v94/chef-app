package com.vojkan.chefapp.ui.searchresult;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vojkan.chefapp.network.BaseTask;
import com.vojkan.chefapp.network.TaskRunner;
import com.vojkan.chefapp.recipedata.Recipe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SearchResultViewModel extends ViewModel {

    private TaskRunner taskRunner = new TaskRunner();
    private List<Recipe> recipes = new ArrayList<>();
    private MutableLiveData<String> title;
    private MutableLiveData<Boolean> loadedMutableLiveData, errorOccurred;
    private JSONArray recipesJSON;
    private int offset = 0, totalResults = 0;

    public SearchResultViewModel() {
        title = new MutableLiveData<>();
        loadedMutableLiveData = new MutableLiveData<>();
        errorOccurred = new MutableLiveData<>();
    }

    public void loadMoreRecipes() {
        parseJSONResponse(recipesJSON);
    }

    public void search(String mealType) {
        loadRecipesByMealType(mealType, offset * 10);
        offset++;
    }

    public void loadRecipesByIngredients(String ingredients) {
        final String URL = "https://api.spoonacular.com/recipes/findByIngredients?ingredients=" + ingredients + "&number=100&apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new Task(URL));
    }

    public void loadRecipesByMealType(String mealType, int offset) {
        final String URL = "https://api.spoonacular.com/recipes/complexSearch?type=" + mealType + "&offset=" + offset + "&number=10&apiKey=916e26022c804c5a9957198f0e7cdf9e";
        taskRunner.executeAsync(new Task(URL));
    }

    private class Task extends BaseTask<String> {

        final String stringUrl;

        public Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int dataStreamReader = inputStreamReader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataStreamReader != -1) {
                    char current = (char) dataStreamReader;
                    stringBuilder.append(current);
                    dataStreamReader = inputStreamReader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                if (stringUrl.contains("type")) {
                    parseJSONResponse(result);
                } else {
                    recipesJSON = new JSONArray(result);
                    parseJSONResponse(recipesJSON);
                }
            } catch (Exception e) {
                e.printStackTrace();
                errorOccurred.setValue(true);
            } finally {
                loadedMutableLiveData.setValue(true);
            }
        }
    }

    private void parseJSONResponse(JSONArray recipesJSON) {
        try {
            JSONObject recipeJSON;
            int a = getOffset() * 10;
            int b = getOffset() * 10 + 10;
            for (int i = a; i < b; i++) {
                recipeJSON = recipesJSON.getJSONObject(i);
                Recipe recipe = new Recipe();
                recipe.setId(recipeJSON.getInt("id"));
                recipe.setName(recipeJSON.getString("title"));
                recipe.setImageUrl(recipeJSON.getString("image"));
                recipes.add(recipe);
            }
            totalResults = recipesJSON.length();
            offset++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseJSONResponse(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray recipesJSON = response.getJSONArray("results");
            JSONObject recipeJSON;
            for (int i = 0; i < recipesJSON.length(); i++) {
                recipeJSON = recipesJSON.getJSONObject(i);
                Recipe recipe = new Recipe();
                recipe.setId(recipeJSON.getInt("id"));
                recipe.setName(recipeJSON.getString("title"));
                recipe.setImageUrl(recipeJSON.getString("image"));
                recipes.add(recipe);
            }
            totalResults =  response.getInt("totalResults");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
