package com.vojkan.chefapp.ui.settings;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.vojkan.chefapp.R;

public class SettingsFragment extends AppCompatActivity {

    SwitchMaterial darkTheme;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);

        darkTheme = findViewById(R.id.switch_theme_dark);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            darkTheme.setChecked(true);
        }

        darkTheme.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                restartApp();
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                restartApp();
            }
        });
    }

    private void restartApp() {
        Intent intent = new Intent(getApplicationContext(), SettingsFragment.class);
        startActivity(intent);
        finish();
    }

}