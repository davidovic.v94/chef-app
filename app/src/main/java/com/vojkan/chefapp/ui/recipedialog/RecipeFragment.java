package com.vojkan.chefapp.ui.recipedialog;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.recipedata.Equipment;
import com.vojkan.chefapp.recipedata.Ingredient;
import com.vojkan.chefapp.recipedata.Instruction;
import com.vojkan.chefapp.recipedata.Nutrient;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.utils.CircleTransform;
import com.vojkan.chefapp.utils.CustomAnimations;
import com.vojkan.chefapp.utils.ProgressDialogFragment;
import com.vojkan.chefapp.utils.SlideAnimation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class RecipeFragment extends DialogFragment {

    private ScrollView scrollView;
    private TextView recipeTitle, servings, readyTime, summaryTitle, summaryText, ingredientsTitle,
            equipmentsTitle, instructionsTitle, costPerServing;
    private ImageView recipeImage;
    private LinearLayout ingredientListHolder, equipmentListHolder, recipeInstructionsHolder,
            tasteChartHolder, nutritionChartHolder, priceChartHolder, nutritionQuickViewLayout,
            recipeLayoutHolder;
    private MaterialButton tasteButton, nutritionButton, priceButton;

    private CustomAnimations customAnimations;
    private final Set<Equipment> equipments = new HashSet<>();
    private int recipeId;
    private BarChart barChart;
    private RadarChart radarChart;
    private PieChart pieChart;
    private Double totalCost = 0.0;

    private final ArrayList<RadarEntry> radarEntries = new ArrayList<>();
    private final ArrayList<BarEntry> barEntriesGoodNutrients = new ArrayList<>();
    private final ArrayList<BarEntry> barEntriesBadNutrients = new ArrayList<>();
    private final ArrayList<PieEntry> pieEntries = new ArrayList<>();
    private final List<String> barChartLabels = new ArrayList<>();

    public ProgressDialogFragment progressDialogFragment;

    public static final int[] PIE_CHART_COLORS = {
            Color.rgb(192, 102, 102), Color.rgb(255, 178, 102), Color.rgb(255, 255, 102),
            Color.rgb(178, 255, 102), Color.rgb(102, 255, 102), Color.rgb(102, 102, 178),
            Color.rgb(102, 255, 255), Color.rgb(102, 178, 255), Color.rgb(102, 102, 255),
            Color.rgb(178, 102, 255), Color.rgb(255, 102, 255), Color.rgb(255, 102, 178),
    };

    public static RecipeFragment newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt("recipeId", id);

        RecipeFragment fragment = new RecipeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_MaterialComponents_Light_DialogWhenLarge);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_ChefApp_NoActionBar_FullScreenDialog);
        if (getArguments() != null) {
            recipeId = getArguments().getInt("recipeId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("onCreateView: ", "onCreateView: ==============================================================");
        RecipeViewModel recipeViewModel = new ViewModelProvider(this).get(RecipeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_recipe, container, false);

        progressDialogFragment = ProgressDialogFragment.newInstance(getActivity());
        if (getActivity() != null) {
            progressDialogFragment.show(getActivity().getSupportFragmentManager(), "Progress_Dialog");
        }

        recipeViewModel.loadRecipes(recipeId);
        recipeViewModel.getTaste(recipeId);
        recipeViewModel.getPriceBreakdown(recipeId);

        scrollView = root.findViewById(R.id.recipe_layout_scrollview);
        recipeLayoutHolder = root.findViewById(R.id.recipe_layout);
        recipeTitle = root.findViewById(R.id.recipe_title);
        recipeImage = root.findViewById(R.id.recipe_image_view);
        servings = root.findViewById(R.id.recipe_servings);
        readyTime = root.findViewById(R.id.recipe_ready_time);
        summaryTitle = root.findViewById(R.id.recipe_summary_title);
        ingredientsTitle = root.findViewById(R.id.recipe_ingredients);
        equipmentsTitle = root.findViewById(R.id.recipe_equipments);
        instructionsTitle = root.findViewById(R.id.recipe_instructions);
        summaryText = root.findViewById(R.id.recipe_summary_text);
        costPerServing = root.findViewById(R.id.price_per_serving_text);
        ingredientListHolder = root.findViewById(R.id.ingredients_list_holder);
        equipmentListHolder = root.findViewById(R.id.equipment_list_holder);
        recipeInstructionsHolder = root.findViewById(R.id.recipe_instruction_holder);
        tasteButton = root.findViewById(R.id.recipe_button_taste);
        nutritionButton = root.findViewById(R.id.recipe_button_nutrition);
        priceButton = root.findViewById(R.id.recipe_button_price);
        nutritionQuickViewLayout = root.findViewById(R.id.nutrition_quickview_layout);

        tasteChartHolder = root.findViewById(R.id.taste_radar_chart_holder);
        nutritionChartHolder = root.findViewById(R.id.nutrition_bar_chart_holder);
        priceChartHolder = root.findViewById(R.id.price_pie_chart_holder);

        barChart = root.findViewById(R.id.nutrition_bar_chart);
        radarChart = root.findViewById(R.id.taste_radar_chart);
        pieChart = root.findViewById(R.id.price_pie_chart);

        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
        }

        recipeViewModel.getErrorOccurred().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });

        recipeViewModel.getLoadedMutableLiveData().observe(getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean) {
                progressDialogFragment.dismiss();
                if (recipeLayoutHolder.getVisibility() == View.INVISIBLE) {
                    recipeLayoutHolder.setVisibility(View.VISIBLE);
                }
            }
        });

        recipeViewModel.getRecipeMutableLiveData().observe(getViewLifecycleOwner(), recipe -> {
            recipeTitle.setText(recipe.getName());
            Picasso.get().load(recipe.getImageUrl())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.no_image_available_image)
                    .transform(new CircleTransform(50, 0))
                    .into(recipeImage);
            servings.setText(String.format(Locale.getDefault(), "%d persons", recipe.getServings()));
            readyTime.setText(String.format(Locale.getDefault(), "%s min", recipe.getTime()));

            if (recipe.getSummary() != null) {
                summaryText.setText(Html.fromHtml(recipe.getSummary(), Html.FROM_HTML_MODE_LEGACY).toString());
            } else {
                summaryTitle.setVisibility(View.GONE);
            }

            getIngredients(inflater, recipe);
            getInstructions(inflater, recipe);
            getEquipments(inflater);
            getNutritionQuickView(recipe);

            List<Nutrient> nutrientArrayList = new ArrayList<>(recipe.getNutrients());

            for (int i = 0; i < nutrientArrayList.size(); i++) {
                if (i < 7) {
                    barEntriesBadNutrients.add(new BarEntry(i, nutrientArrayList.get(i).getPercentOfDailyNeeds().floatValue()));
                } else {
                    barEntriesGoodNutrients.add(new BarEntry(i, nutrientArrayList.get(i).getPercentOfDailyNeeds().floatValue()));
                }
                if (nutrientArrayList.get(i).getAmount() % 1 == 0) {
                    barChartLabels.add(String.format(Locale.getDefault(), " %s (%d%s)", nutrientArrayList.get(i).getTitle(),
                            nutrientArrayList.get(i).getAmount().intValue(), nutrientArrayList.get(i).getUnit()));
                } else {
                    barChartLabels.add(String.format(Locale.getDefault(), " %s (%.2f%s)", nutrientArrayList.get(i).getTitle(),
                            nutrientArrayList.get(i).getAmount().floatValue(), nutrientArrayList.get(i).getUnit()));
                }
            }

            barChart.notifyDataSetChanged();
            barChart.invalidate();
            barChart.refreshDrawableState();

        });

        recipeViewModel.getTasteMutableLiveData().observe(getViewLifecycleOwner(), taste -> {
            radarEntries.add(new RadarEntry(taste.getSweetness().floatValue()));
            radarEntries.add(new RadarEntry(taste.getSaltiness().floatValue()));
            radarEntries.add(new RadarEntry(taste.getSourness().floatValue()));
            radarEntries.add(new RadarEntry(taste.getBitterness().floatValue()));
            radarEntries.add(new RadarEntry(taste.getSavoriness().floatValue()));
            radarEntries.add(new RadarEntry(taste.getFattiness().floatValue()));

            radarChart.notifyDataSetChanged();
            radarChart.invalidate();
            radarChart.refreshDrawableState();
        });

        recipeViewModel.getIngredientsMutableLiveData().observe(getViewLifecycleOwner(), ingredients -> {
            for (int i = 0; i < ingredients.size(); i++) {
                String ingredientTitle = ingredients.get(i).getName();
                pieEntries.add(new PieEntry(ingredients.get(i).getPrice().floatValue(), ingredientTitle));
            }

            getPrices(inflater, ingredients);
            pieChart.notifyDataSetChanged();
            pieChart.invalidate();
            pieChart.refreshDrawableState();
        });

        recipeViewModel.getCostPerServing().observe(getViewLifecycleOwner(),
                aDouble -> costPerServing.setText(String.format(Locale.getDefault(), "Cost per serving: $%.2f", aDouble.floatValue())));

        tasteButton.setOnClickListener(view -> {
            final int TASTE_CHART_HOLDER_HEIGHT_PX = 788;
            if (tasteChartHolder.getVisibility() == View.VISIBLE) {
                tasteButton.setText(R.string.recipe_button_taste_show);

                Animation animation = new SlideAnimation(tasteChartHolder, TASTE_CHART_HOLDER_HEIGHT_PX, 0);
//                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(1000);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        tasteChartHolder.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                tasteChartHolder.setAnimation(animation);
                tasteChartHolder.startAnimation(animation);
            } else {
                getTasteData();
                tasteButton.setText(R.string.recipe_button_taste_hide);

                Animation animation = new SlideAnimation(tasteChartHolder, 0, TASTE_CHART_HOLDER_HEIGHT_PX);
//                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(1000);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        tasteChartHolder.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                tasteChartHolder.setAnimation(animation);
                tasteChartHolder.startAnimation(animation);
            }
        });

        nutritionButton.setOnClickListener(view -> {
            final int NUTRITION_CHART_HOLDER_HEIGHT_PX = 1479;
            if (nutritionChartHolder.getVisibility() == View.VISIBLE) {
                nutritionButton.setText(R.string.recipe_button_nutrition_show);

                Animation animation = new SlideAnimation(nutritionChartHolder, NUTRITION_CHART_HOLDER_HEIGHT_PX, 0);
//                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(1000);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        nutritionChartHolder.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                nutritionChartHolder.setAnimation(animation);
                nutritionChartHolder.startAnimation(animation);
            } else {
                getNutritionData();
                nutritionButton.setText(R.string.recipe_button_nutrition_hide);

                Animation animation = new SlideAnimation(nutritionChartHolder, 0, NUTRITION_CHART_HOLDER_HEIGHT_PX);
//                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(1000);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        nutritionChartHolder.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                nutritionChartHolder.setAnimation(animation);
                nutritionChartHolder.startAnimation(animation);
            }
        });

        priceButton.setOnClickListener(view -> {
            int PRICE_CHART_HOLDER_HEIGHT_PX = (int) (326 * getResources().getDisplayMetrics().density)
                    + (int) (38 * getResources().getDisplayMetrics().density) * pieEntries.size();
            if (priceChartHolder.getVisibility() == View.VISIBLE) {
                priceButton.setText(R.string.recipe_button_price_show);

                Animation animation = new SlideAnimation(priceChartHolder, PRICE_CHART_HOLDER_HEIGHT_PX, 0);
//                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(1000);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        priceChartHolder.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                priceChartHolder.setAnimation(animation);
                priceChartHolder.startAnimation(animation);
            } else {
                getPriceData();
                priceButton.setText(R.string.recipe_button_price_hide);

                Animation animation = new SlideAnimation(priceChartHolder, 0, PRICE_CHART_HOLDER_HEIGHT_PX);
//                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(1000);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        priceChartHolder.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                priceChartHolder.setAnimation(animation);
                priceChartHolder.startAnimation(animation);
            }
        });

        root.setFocusableInTouchMode(true);
        root.requestFocus();
        root.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                customAnimations.hideSplitUpAnimation(this, scrollView, recipeLayoutHolder);
                return true;
            }
            return false;
        });

        return root;
    }

    private void getPrices(LayoutInflater layoutInflater, List<Ingredient> ingredients) {
        LinearLayout priceBreakdownLayout = (LinearLayout) layoutInflater.inflate(R.layout.ingredient_price_breakdown_layout, priceChartHolder, false);
        LinearLayout ingredientPriceItemHolder = priceBreakdownLayout.findViewById(R.id.ingredient_price_item_holder);
        TextView totalPrice = priceBreakdownLayout.findViewById(R.id.ingredient_total_price);

        for (Ingredient ingredient : ingredients) {
            View ingredientPriceItem = layoutInflater.inflate(R.layout.ingredient_price_layout, ingredientPriceItemHolder, false);
            TextView ingredientTitle = ingredientPriceItem.findViewById(R.id.ingredient_title);
            TextView ingredientPrice = ingredientPriceItem.findViewById(R.id.ingredient_price);

            if (ingredient.getAmount() % 1 == 0) {
                ingredientTitle.setText(String.format(Locale.getDefault(), "%d %s %s", ingredient.getAmount().intValue(),
                        ingredient.getUnit(), ingredient.getName()));
            } else {
                ingredientTitle.setText(String.format(Locale.getDefault(), "%.2f %s %s", ingredient.getAmount().floatValue(),
                        ingredient.getUnit(), ingredient.getName()));
            }

            ingredientPrice.setText(String.format(Locale.getDefault(), "$%.2f", ingredient.getPrice().floatValue()));
            totalCost += ingredient.getPrice();

            ingredientPriceItemHolder.addView(ingredientPriceItem);
        }

        totalPrice.setText(String.format(Locale.getDefault(), "$%.2f", totalCost.floatValue()));

        priceChartHolder.addView(priceBreakdownLayout);
    }

    private void getNutritionQuickView(Recipe recipe) {
        TextView totalCalories = nutritionQuickViewLayout.findViewById(R.id.nutrition_quickview_calories);
        TextView totalProtein = nutritionQuickViewLayout.findViewById(R.id.nutrition_quickview_protein);
        TextView totalFat = nutritionQuickViewLayout.findViewById(R.id.nutrition_quickview_total_fat);
        TextView totalCarbohydrates = nutritionQuickViewLayout.findViewById(R.id.nutrition_quickview_carbs);
        TextView healthScore = nutritionQuickViewLayout.findViewById(R.id.nutrition_quickview_health_score);

        int i = 0;

        for (Nutrient nutrient : recipe.getNutrients()) {
            if (i == 4) {
                break;
            }
            switch (nutrient.getTitle()) {
                case "Calories":
                    totalCalories.setText(String.format(Locale.getDefault(), "%d %s", nutrient.getAmount().intValue(), nutrient.getTitle()));
                    i++;
                    break;
                case "Fat":
                    totalFat.setText(String.format(Locale.getDefault(), "%d%s %s", nutrient.getAmount().intValue(), nutrient.getUnit(), nutrient.getTitle()));
                    i++;
                    break;
                case "Carbohydrates":
                    totalCarbohydrates.setText(String.format(Locale.getDefault(), "%d%s Carbs", nutrient.getAmount().intValue(), nutrient.getUnit()));
                    i++;
                    break;
                case "Protein":
                    totalProtein.setText(String.format(Locale.getDefault(), "%d%s %s", nutrient.getAmount().intValue(), nutrient.getUnit(), nutrient.getTitle()));
                    i++;
                    break;
            }
        }

        healthScore.setText(String.format(Locale.getDefault(), "%d%% HS", recipe.getHealthScore().intValue()));
    }

    private void getIngredients(LayoutInflater layoutInflater, Recipe recipe) {
        if (recipe.getIngredients().size() == 0) {
            ingredientsTitle.setVisibility(View.GONE);
            return;
        }

        for (Ingredient ingredient : recipe.getIngredients()) {
            View ingredientItem = layoutInflater.inflate(R.layout.ingredient_horizontal_scroll_view_item, ingredientListHolder, false);
            TextView measure = ingredientItem.findViewById(R.id.ingredient_item_measure);
            ImageView icon = ingredientItem.findViewById(R.id.ingredient_item_icon);
            TextView name = ingredientItem.findViewById(R.id.ingredient_item_name);

            if (ingredient.getAmount() % 1 == 0) {
                measure.setText(String.format(Locale.getDefault(), "%d %s", ingredient.getAmount().intValue(), ingredient.getUnit()));
            } else {
                measure.setText(String.format(Locale.getDefault(), "%.2f %s", ingredient.getAmount().floatValue(), ingredient.getUnit()));
            }

            String url = "https://spoonacular.com/cdn/ingredients_100x100/" + ingredient.getImageFile();
            Picasso.get().load(url)
                    .fit()
//                    .centerCrop()
                    .centerInside()
                    .error(R.drawable.no_image_available_image)
//                    .transform(new CircleTransform(50, 0))
                    .into(icon);
            name.setText(ingredient.getName());

            ingredientListHolder.addView(ingredientItem);
        }
    }

    private void getInstructions(LayoutInflater layoutInflater, Recipe recipe) {
        int br = 1;
        for (Instruction instruction : recipe.getAnalyzedInstructions()) {
            equipments.addAll(instruction.getEquipments());

            View instructionItem = layoutInflater.inflate(R.layout.instruction_item_layout, recipeInstructionsHolder, false);
            TextView step = instructionItem.findViewById(R.id.instruction_item_step);
            TextView text = instructionItem.findViewById(R.id.instruction_item_text);

            step.setText(String.format(Locale.getDefault(), "%d. step ", br));
            text.setText(instruction.getStep());

            recipeInstructionsHolder.addView(instructionItem);
            br++;
        }

        if (recipe.getAnalyzedInstructions().size() == 0) {
            instructionsTitle.setVisibility(View.GONE);
        }
    }

    private void getEquipments(LayoutInflater layoutInflater) {
        if (equipments.size() == 0) {
            equipmentsTitle.setVisibility(View.GONE);
        }

        for (Equipment equipment : equipments) {
            View equipmentItem = layoutInflater.inflate(R.layout.equipment_horizontal_scroll_view_item, equipmentListHolder, false);
            ImageView icon = equipmentItem.findViewById(R.id.equipment_item_icon);
            TextView name = equipmentItem.findViewById(R.id.equipment_item_name);

            String url = "https://spoonacular.com/cdn/equipment_100x100/" + equipment.getImageFile();
            Picasso.get().load(url)
                    .fit()
//                    .centerCrop()
                    .centerInside()
                    .error(R.drawable.no_image_available_image)
//                    .transform(new CircleTransform(50, 0))
                    .into(icon);
            name.setText(equipment.getName());

            equipmentListHolder.addView(equipmentItem);
        }
    }

    private void getTasteData() {
        RadarDataSet radarDataSet = new RadarDataSet(radarEntries, "Taste");
        radarDataSet.setColor(Color.RED);
        radarDataSet.setLineWidth(2f);
//        radarDataSet.setValueTextColor(Color.RED);
        radarDataSet.setDrawValues(false);
        radarDataSet.setFillColor(Color.RED);
        radarDataSet.setDrawFilled(true);

        RadarData radarData = new RadarData();
        radarData.addDataSet(radarDataSet);

        String[] labels = {"Sweet", "Salty", "Sour", "Bitter", "Savory", "Fatty"};
        XAxis xAxis = radarChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
        xAxis.setTextSize(14f);

        YAxis yAxis = radarChart.getYAxis();
        yAxis.setDrawLabels(false);
//        yAxis.setAxisMaximum(80);

        radarChart.setData(radarData);
        radarChart.getLegend().setEnabled(false);
        Description description = new Description();
        description.setText("Recipe taste chart");
        description.setTextSize(10f);
        description.setYOffset(-12f);
        radarChart.setDescription(description);
        radarChart.setWebLineWidth(2f);
        radarChart.setWebLineWidthInner(2f);
//        radarChart.setClickable(false);
//        radarChart.setTouchEnabled(false);
        radarChart.animateY(1000);
    }

    private void getNutritionData() {
        BarDataSet barDataSetGoodNutrients = new BarDataSet(barEntriesGoodNutrients, "get enough of these");
        barDataSetGoodNutrients.setColor(Color.BLUE);
        barDataSetGoodNutrients.setValueTextColor(Color.BLACK);
        barDataSetGoodNutrients.setValueTextSize(14f);

        BarDataSet barDataSetBadNutrients = new BarDataSet(barEntriesBadNutrients, "limit these");
        barDataSetBadNutrients.setColor(Color.RED);
        barDataSetBadNutrients.setValueTextColor(Color.BLACK);
        barDataSetBadNutrients.setValueTextSize(14f);

        BarData barData = new BarData(barDataSetBadNutrients, barDataSetGoodNutrients);
        barData.setValueFormatter(new PercentFormatter());

        barChart.setFitBars(true);
        barChart.setData(barData);
        Description description = new Description();
        description.setText("Covered percent of daily need");
        description.setTextSize(10f);
        description.setYOffset(-12f);
        barChart.setDescription(description);
        barChart.animateY(1000);

        BarChartMarkerView bmv = new BarChartMarkerView(getContext(), R.layout.marker_view_layout);
        barChart.setMarker(bmv);
        barChart.setDragEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(barChartLabels));
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);
//        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(barChartLabels.size());
        xAxis.setLabelRotationAngle(270);
    }

    private void getPriceData() {
        PieDataSet pieDataSet = new PieDataSet(pieEntries, "Ingredients");
        pieDataSet.setColors(PIE_CHART_COLORS);
        pieDataSet.setValueTextColor(Color.BLACK);
        pieDataSet.setValueTextSize(14f);
        pieDataSet.setDrawValues(false);
        pieDataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData pieData = new PieData(pieDataSet);
        pieChart.setEntryLabelColor(Color.BLACK);

        pieChart.setData(pieData);
        pieChart.getDescription().setEnabled(false);
//        pieChart.setCenterText("");
        pieChart.setHoleColor(Color.TRANSPARENT);
        pieChart.animateXY(1000, 1000);
        pieChart.getLegend().setEnabled(false);

        PieChartMarkerView pmv = new PieChartMarkerView(getContext(), R.layout.marker_view_layout);
        pieChart.setMarker(pmv);

//        Legend legend = pieChart.getLegend();
//        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
//        legend.setDrawInside(false);
    }

    public class BarChartMarkerView extends MarkerView {

        private final TextView tvContent;

        public BarChartMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);
            tvContent = findViewById(R.id.tvContent);
        }

        @Override
        public void refreshContent(Entry e, Highlight highlight) {
            super.refreshContent(e, highlight);
            for (int i = 0; i < barChartLabels.size(); i++) {
                if (e.getX() == i) {
                    tvContent.setText(String.format(Locale.getDefault(), "%s - %.1f%%", barChartLabels.get(i), e.getY()));
                    return;
                }
            }
        }

        @Override
        public void draw(Canvas canvas, float posX, float posY) {
            float w = getWidth();
            float h = getHeight();
            float wp = nutritionChartHolder.getWidth();

            posY -= h / 3 * 5;

            if (posX < w / 2) {
                canvas.translate(0, posY);
            } else if ((posX > w / 2) && posX < (wp - w / 2)) {
                posX = posX - w / 2;
                canvas.translate(posX, posY);
            } else {
                posX = wp - w;
                canvas.translate(posX, posY);
            }

            draw(canvas);
        }

    }

    public class PieChartMarkerView extends MarkerView {

        private final TextView tvContent;

        public PieChartMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);
            tvContent = findViewById(R.id.tvContent);
        }

        @Override
        public void refreshContent(Entry e, Highlight highlight) {
            super.refreshContent(e, highlight);
            for (PieEntry pieEntry : pieEntries) {
                if (pieEntry.equals(e)) {
                    tvContent.setText(String.format(Locale.getDefault(), "%s $%.2f", pieEntry.getLabel(), pieEntry.getValue()));
                    return;
                }
            }
        }

        @Override
        public void draw(Canvas canvas, float posX, float posY) {
            float w = getWidth();
//            float h = getHeight();
            float wp = priceChartHolder.getWidth();

//            posY -= h/3 * 5;

            if (posX < w / 2) {
                canvas.translate(0, posY);
            } else if ((posX > w / 2) && posX < (wp - w / 2)) {
                posX = posX - w / 2;
                canvas.translate(posX, posY);
            } else {
                posX = wp - w;
                canvas.translate(posX, posY);
            }

            draw(canvas);
        }

    }

}