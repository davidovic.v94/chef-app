package com.vojkan.chefapp.ui.havenoidea;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.utils.CustomAnimations;

import java.util.Random;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class HaveNoIdeaFragment extends Fragment {

    private CustomAnimations customAnimations;
    private TextView textViewResult;
    private ImageView imageViewWheel, imageViewPointer;
    private MaterialButton buttonSpin, buttonFindRecipes;
    private Random r;
    private int degrees = 0, degreesOld = 0;
    private static final float FACTOR = 11.25f;
    private String text;
    private KonfettiView konfettiView;
    private View circularRevealingView;

    private final int FIND_RECIPES_BUTTON_CODE = 3;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_have_no_idea, container, false);

        imageViewWheel = root.findViewById(R.id.have_no_idea_spinning_wheel);
        imageViewPointer = root.findViewById(R.id.have_no_idea_pointer);
        buttonSpin = root.findViewById(R.id.have_no_idea_start_button);
        buttonFindRecipes = root.findViewById(R.id.have_no_idea_find_recipes_button);
        textViewResult = root.findViewById(R.id.text_view_result);
        konfettiView = root.findViewById(R.id.confetti_view);
        circularRevealingView = root.findViewById(R.id.circular_reveal_view);

        if (getActivity() != null) {
            customAnimations = new CustomAnimations(getActivity());
        }

        r = new Random();

        buttonSpin.setOnClickListener(view -> {
            if (konfettiView.isActive()) {
                konfettiView.stopGracefully();
            }

            animateFoodWheel();
            animateWheelPointer();
        });

        buttonFindRecipes.setOnClickListener(view -> {
            if (text == null) {
                Toast.makeText(getContext(), "Error! Spin again", Toast.LENGTH_SHORT).show();
                return;
            }
            customAnimations.circularRevealView(circularRevealingView, FIND_RECIPES_BUTTON_CODE, text);
        });

        return root;
    }

    private String currentMealType(int degrees) {
        if (degrees >= FACTOR * 1 && degrees < FACTOR * 3) {
            text = "Appetizer";
        } else if (degrees >= FACTOR * 3 && degrees < FACTOR * 5) {
            text = "Soup";
        } else if (degrees >= FACTOR * 5 && degrees < FACTOR * 7) {
            text = "Dessert";
        } else if (degrees >= FACTOR * 7 && degrees < FACTOR * 9) {
            text = "Salad";
        } else if (degrees >= FACTOR * 9 && degrees < FACTOR * 11) {
            text = "Breakfast";
        } else if (degrees >= FACTOR * 11 && degrees < FACTOR * 13) {
            text = "Bread";
        } else if (degrees >= FACTOR * 13 && degrees < FACTOR * 15) {
            text = "Snack";
        } else if (degrees >= FACTOR * 15 && degrees < FACTOR * 17) {
            text = "Finger Food";
        } else if (degrees >= FACTOR * 17 && degrees < FACTOR * 19) {
            text = "Side Dish";
        } else if (degrees >= FACTOR * 19 && degrees < FACTOR * 21) {
            text = "Beverage";
        } else if (degrees >= FACTOR * 21 && degrees < FACTOR * 23) {
            text = "Vegetable";
        } else if (degrees >= FACTOR * 23 && degrees < FACTOR * 25) {
            text = "Marinade";
        } else if (degrees >= FACTOR * 25 && degrees < FACTOR * 27) {
            text = "Fruit";
        } else if (degrees >= FACTOR * 27 && degrees < FACTOR * 29) {
            text = "Drink";
        } else if (degrees >= FACTOR * 29 && degrees < FACTOR * 31) {
            text = "Sauce";
        } else if ((degrees >= FACTOR * 31 && degrees < 360) || (degrees >= 0 && degrees < FACTOR * 1)) {
            text = "Main course";
        }

        return text;
    }

    private void animateFoodWheel() {
        degreesOld = degrees % 360;
        int randomDeg = r.nextInt(360);
        degrees = randomDeg + 720;

        RotateAnimation rotateAnimation = new RotateAnimation(degreesOld, degrees,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(5000);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                textViewResult.setText("");
                buttonFindRecipes.setVisibility(View.INVISIBLE);
                textViewResult.setVisibility(View.INVISIBLE);
                buttonSpin.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                getConfettiAnimation();
                textViewResult.setText(currentMealType(360 - (degrees % 360)));
                buttonSpin.setVisibility(View.VISIBLE);
                textViewResult.setVisibility(View.VISIBLE);
                buttonFindRecipes.setVisibility(View.VISIBLE);
                buttonSpin.setText(getResources().getString(R.string.have_no_idea_spin_button_again));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageViewWheel.startAnimation(rotateAnimation);
    }

    private void animateWheelPointer() {
        RotateAnimation rotateAnimation = new RotateAnimation(0, -70,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.0f);
        rotateAnimation.setDuration(100);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setRepeatCount((int) ((degrees - degreesOld) / 22.5));
        rotateAnimation.setRepeatMode(Animation.RESTART);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                RotateAnimation rotateAnimation = new RotateAnimation(-70, 0,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.0f);
                rotateAnimation.setDuration(100);
                rotateAnimation.setInterpolator(new DecelerateInterpolator());
                imageViewPointer.startAnimation(rotateAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageViewPointer.startAnimation(rotateAnimation);
    }

    private void getConfettiAnimation() {
        if (getContext() != null) {
            final Drawable chefHat = ContextCompat.getDrawable(getContext(), R.drawable.chef_hat);
            final Drawable baguette = ContextCompat.getDrawable(getContext(), R.drawable.baguette_icon);
            final Drawable muffin = ContextCompat.getDrawable(getContext(), R.drawable.muffin_icon);
            final Drawable strawberry = ContextCompat.getDrawable(getContext(), R.drawable.strawberry_icon);
            final Drawable noodles = ContextCompat.getDrawable(getContext(), R.drawable.noodles_icon);
            final Drawable frenchFries = ContextCompat.getDrawable(getContext(), R.drawable.french_fries_icon);
            final Drawable hamburger = ContextCompat.getDrawable(getContext(), R.drawable.hamburger_icon);
            final Drawable burrito = ContextCompat.getDrawable(getContext(), R.drawable.burrito_icon);

            final Shape.DrawableShape chefHatDrawableShape = new Shape.DrawableShape(chefHat, false);
            final Shape.DrawableShape baguetteDrawableShape = new Shape.DrawableShape(baguette, false);
            final Shape.DrawableShape muffinDrawableShape = new Shape.DrawableShape(muffin, false);
            final Shape.DrawableShape strawberryDrawableShape = new Shape.DrawableShape(strawberry, false);
            final Shape.DrawableShape noodlesDrawableShape = new Shape.DrawableShape(noodles, false);
            final Shape.DrawableShape frenchFriesDrawableShape = new Shape.DrawableShape(frenchFries, false);
            final Shape.DrawableShape hamburgerDrawableShape = new Shape.DrawableShape(hamburger, false);
            final Shape.DrawableShape burritoDrawableShape = new Shape.DrawableShape(burrito, false);

            konfettiView.build()
//                            .addColors(R.color.md_green_700, R.color.md_red_700, R.color.md_blue_700, R.color.md_yellow_700)
//                            .addColors(Color.GREEN, Color.RED, Color.BLUE, Color.YELLOW)
                    .setDirection(30.0, 150.0)
                    .setSpeed(1f, 5f)
                    .setFadeOutEnabled(true)
                    .setTimeToLive(2000L)
                    .addShapes(chefHatDrawableShape, baguetteDrawableShape, muffinDrawableShape, strawberryDrawableShape,
                            noodlesDrawableShape, frenchFriesDrawableShape, hamburgerDrawableShape, burritoDrawableShape)
                    .addSizes(new Size(12, 5f))
                    .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                    .streamFor(300, 3000);
        }
    }

}