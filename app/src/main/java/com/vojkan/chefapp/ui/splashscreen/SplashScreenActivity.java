package com.vojkan.chefapp.ui.splashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        final int SPLASH_SCREEN = 5000;

        AnimationSet as = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0,720, Animation.RELATIVE_TO_SELF,
                0.5f , Animation.RELATIVE_TO_SELF,0.5f );
        rotate.setDuration(2000);
        as.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(0, 0, -1000, 0);
        translate.setDuration(2000);
        as.addAnimation(translate);

        Animation rightAnim = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
        Animation leftAnim = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);

        final ImageView logo = findViewById(R.id.splash_screen_logo);
        TextView title = findViewById(R.id.splash_screen_title);
        TextView welcome = findViewById(R.id.splash_screen_text);

        logo.setAnimation(as);
        logo.startAnimation(as);
        title.setAnimation(rightAnim);
        welcome.setAnimation(leftAnim);

        new Handler().postDelayed(() -> {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }, SPLASH_SCREEN);

    }
}