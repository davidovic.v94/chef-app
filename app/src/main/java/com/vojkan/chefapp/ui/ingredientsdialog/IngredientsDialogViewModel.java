package com.vojkan.chefapp.ui.ingredientsdialog;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.data.DataManage;
import com.vojkan.chefapp.network.BaseTask;
import com.vojkan.chefapp.network.TaskRunner;
import com.vojkan.chefapp.recipedata.Ingredient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class IngredientsDialogViewModel extends ViewModel {

    private final TaskRunner taskRunner = new TaskRunner();
    private MutableLiveData<List<Ingredient>> ingredientListMutableLiveData;
    private MutableLiveData<Boolean> errorOccurred;
    private List<Ingredient> ingredients = new ArrayList<>();
    private DataManage dataManage = new DataManage(MainActivity.sharedPreferences);

    public IngredientsDialogViewModel() {
        ingredientListMutableLiveData = new MutableLiveData<>();
        errorOccurred = new MutableLiveData<>();
    }

    public void loadIngredients() {
        for (char alphabet = 'a'; alphabet <= 'z'; alphabet++) {
            final String URL = "https://api.spoonacular.com/food/ingredients/autocomplete?query=" + alphabet + "&number=100&apiKey=916e26022c804c5a9957198f0e7cdf9e";
            taskRunner.executeAsync(new IngredientsDialogViewModel.Task(URL));
        }
    }

    private class Task extends BaseTask<String> {

        final String stringUrl;

        public Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int dataStreamReader = inputStreamReader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataStreamReader != -1) {
                    char current = (char) dataStreamReader;
                    stringBuilder.append(current);
                    dataStreamReader = inputStreamReader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                parseJSONResponse(result);
                ingredientListMutableLiveData.setValue(ingredients);
                MainActivity.ingredients.addAll(ingredients);
                dataManage.saveData();
            } catch (Exception e) {
                e.printStackTrace();
                errorOccurred.setValue(true);
            }
        }
    }

    private void parseJSONResponse(String result) {
        try {
            JSONArray ingredientsJsonArray = new JSONArray(result);
            if (ingredientsJsonArray.length() > 0) {
                for (int i = 0; i < ingredientsJsonArray.length(); i++) {
                    JSONObject ingredientJsonObject = ingredientsJsonArray.getJSONObject(i);
                    Ingredient ingredient = new Ingredient(ingredientJsonObject.getString("name").toLowerCase(), ingredientJsonObject.getString("image"));
                    ingredients.add(ingredient);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
