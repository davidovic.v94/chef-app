package com.vojkan.chefapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.slider.Slider;
import com.google.android.material.navigation.NavigationView;
import com.vojkan.chefapp.adapters.FilterListAdapter;
import com.vojkan.chefapp.data.DataManage;
import com.vojkan.chefapp.recipedata.Filter;
import com.vojkan.chefapp.recipedata.Ingredient;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.recipedata.enums.Cuisine;
import com.vojkan.chefapp.recipedata.enums.Diet;
import com.vojkan.chefapp.recipedata.enums.Intolerance;
import com.vojkan.chefapp.recipedata.enums.MealType;
import com.vojkan.chefapp.recipedata.enums.Nutrients;
import com.vojkan.chefapp.recipedata.enums.SortBy;
import com.vojkan.chefapp.recipedata.enums.SortOrder;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    public static DrawerLayout drawerLayout;
    public static ActionBarDrawerToggle actionBarDrawerToggle;
    private ScrollView drawerRight;
    public static RecyclerView drawerRightRecyclerView;
    static FragmentManager fragmentManager;
    private NavigationView navigationView;
    private static final List<Filter> filterList = new ArrayList<>();
    public static final List<Recipe> favoriteRecipes = new ArrayList<>();

    public static final Set<Ingredient> ingredients = new TreeSet<>(Comparator.comparing(Ingredient::getName));
    public static SharedPreferences sharedPreferences;

    private static int mToolbarHeight;
    private static ValueAnimator mVaActionBar;

    public static int marginTop;
    public static int marginLeft;

    public static boolean instructionsRequired;
    public static boolean addRecipeInformation;
    public static boolean addRecipeNutrition;
    public static int maxReadyTime;
    public static String sortBy;
    public static String sortOrder;
    public static StringBuilder nutrition = new StringBuilder();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        drawerRight = findViewById(R.id.drawer_right);
        LinearLayout drawerRightLinearLayout = findViewById(R.id.drawer_right_linear_layout);
        drawerRightRecyclerView = findViewById(R.id.drawer_right_recycler_view);

        sharedPreferences = getSharedPreferences("com.vojkan.chefapp", Context.MODE_PRIVATE);
        DataManage dataManage = new DataManage(sharedPreferences);

        dataManage.loadData();

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        fragmentManager = getSupportFragmentManager();

        navigationView.setItemIconTintList(null);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.syncState();
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_find_recipes, R.id.nav_whats_in_your_fridge,
                R.id.nav_have_no_idea, R.id.nav_ask_me_something, R.id.nav_did_you_know, R.id.nav_favorite_recipes)
                .setDrawerLayout(drawerLayout)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        actionBarDrawerToggle.setToolbarNavigationClickListener(view -> {
            if (drawerLayout.isDrawerOpen(navigationView)) {
                drawerLayout.closeDrawer(navigationView);
            } else if (!drawerLayout.isDrawerOpen(navigationView)) {
                drawerLayout.openDrawer(navigationView);
            }
            if (drawerLayout.isDrawerOpen(drawerRight)) {
                drawerLayout.closeDrawer(drawerRight);
            }
            hideKeyboard(this);
        });

        getRightDrawerData(this, drawerRightLinearLayout);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        marginTop = (int) ev.getY();
        marginLeft = (int) ev.getX();
        return super.dispatchTouchEvent(ev);
    }

    public void getRightDrawerData(Activity activity, ViewGroup root) {
        filterList.add(new Filter(getResources().getString(R.string.filter_group_cuisine), Arrays.asList(Cuisine.values())));
        filterList.add(new Filter(getResources().getString(R.string.filter_group_intolerance), Arrays.asList(Intolerance.values())));
        filterList.add(new Filter(getResources().getString(R.string.filter_group_diet), Arrays.asList(Diet.values())));
        filterList.add(new Filter(getResources().getString(R.string.filter_group_meal_type), Arrays.asList(MealType.values())));
        filterList.add(new Filter(getResources().getString(R.string.filter_group_macro_and_micro_nutrients), Arrays.asList(Nutrients.values())));
        FilterListAdapter filterListAdapter = new FilterListAdapter(activity, filterList);

        addCheckboxFilterToDrawer(activity, root, getResources().getString(R.string.drawer_right_instructions_required));
        addCheckboxFilterToDrawer(activity, root, getResources().getString(R.string.drawer_right_recipe_information));
        addCheckboxFilterToDrawer(activity, root, getResources().getString(R.string.drawer_right_recipe_nutrition));
        addSliderFilterToDrawer(activity, root);
        addSpinnerFilterToDrawer(activity, root, getResources().getString(R.string.drawer_right_sort_by));
        addSpinnerFilterToDrawer(activity, root, getResources().getString(R.string.drawer_right_sort_order));
        addButtonSearchToDrawer(activity, root);

        drawerRightRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
        drawerRightRecyclerView.setAdapter(filterListAdapter);
    }

    private void addCheckboxFilterToDrawer(Context context, ViewGroup root, String title) {
        MaterialCheckBox checkBox = (MaterialCheckBox) LayoutInflater
                .from(context)
                .inflate(R.layout.checkbox_item, root, false);
        checkBox.setText(title);
        root.addView(checkBox);

        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            CheckBox cb = (CheckBox) compoundButton;
            switch (cb.getText().toString()) {
                case "Instructions Required":
                    instructionsRequired = b;
                    break;
                case "Add Recipe Information":
                    addRecipeInformation = b;
                    break;
                case "Add Recipe Nutrition":
                    addRecipeNutrition = b;
                    break;
            }
        });
    }

    private void addSliderFilterToDrawer(Context context, ViewGroup root) {
        LinearLayout sliderLayout = (LinearLayout) LayoutInflater
                .from(context)
                .inflate(R.layout.one_side_slider, root, false);
        TextView titleTextView = sliderLayout.findViewById(R.id.slider_item_title);
        Slider slider = sliderLayout.findViewById(R.id.slider_item_slider);

        slider.setLabelFormatter(value -> String.format(Locale.getDefault(), "%d min", (int) value));
        slider.addOnChangeListener((slider1, value, fromUser) -> maxReadyTime = (int) value);

        titleTextView.setText(getResources().getString(R.string.drawer_right_max_time));
        root.addView(sliderLayout);
    }

    private void addSpinnerFilterToDrawer(Context context, ViewGroup root, String title) {
        LinearLayout spinnerLayout = (LinearLayout) LayoutInflater
                .from(context)
                .inflate(R.layout.spinner_item, root, false);
        TextView titleTextView = spinnerLayout.findViewById(R.id.spinner_item_title);
        AppCompatSpinner spinner = spinnerLayout.findViewById(R.id.spinner_item_spinner);
        spinner.setDropDownWidth(744);

        if (title.equals(getResources().getString(R.string.drawer_right_sort_by))) {
            List<String> stringList = Stream.of(SortBy.values())
                    .map(SortBy::name)
                    .map(s -> s = StringUtils.capitalize(s.toLowerCase().replace("_", "-")))
                    .collect(Collectors.toList());
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(context,
                    R.layout.custom_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
            spinner.setAdapter(spinnerArrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (stringList.get(i).equals("None")) {
                        sortBy = "None";
                    } else {
                        sortBy = stringList.get(i).toLowerCase();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else if (title.equals(getResources().getString(R.string.drawer_right_sort_order))) {
            List<String> stringList = Stream.of(SortOrder.values())
                    .map(SortOrder::name)
                    .map(s -> s = StringUtils.capitalize(s.toLowerCase()))
                    .collect(Collectors.toList());
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(context,
                    R.layout.custom_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
            spinner.setAdapter(spinnerArrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (stringList.get(i).equals("Ascending")) {
                        sortOrder = "asc";
                    } else if (stringList.get(i).equals("Descending")) {
                        sortOrder = "desc";
                    } else {
                        sortOrder = "None";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        titleTextView.setText(title);
        root.addView(spinnerLayout);
    }

    private void addButtonSearchToDrawer(Context context, ViewGroup root) {
        float density = getResources().getDisplayMetrics().density;
        MaterialButton buttonSearch = new MaterialButton(context);
        buttonSearch.setId(R.id.filter_search_button);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins((int) density * 8, 0, (int) density * 8, 0);
        buttonSearch.setLayoutParams(params);
        buttonSearch.setCornerRadius(50);
        buttonSearch.setBackgroundColor(getResources().getColor(R.color.md_red_700, null));
        buttonSearch.setIconResource(R.drawable.chef_hat);
        buttonSearch.setIconGravity(MaterialButton.ICON_GRAVITY_TEXT_START);
        buttonSearch.setIconTint(null);
        buttonSearch.setText(R.string.search_recipes);
        root.addView(buttonSearch);
    }

    public View.OnClickListener filterButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (drawerLayout.isDrawerOpen(drawerRight)) {
                drawerLayout.closeDrawer(drawerRight);
            } else if (!drawerLayout.isDrawerOpen(drawerRight)) {
                drawerLayout.openDrawer(drawerRight);
            }
            if (drawerLayout.isDrawerOpen(navigationView)) {
                drawerLayout.closeDrawer(navigationView);
            }
            hideKeyboard(MainActivity.this);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_settings:
//                Intent intent = new Intent(getApplicationContext(), SettingsFragment.class);
//                startActivity(intent);
//                return true;
//            default:
//                break;
//        }
//        return false;
//    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public static String getLastFragment() {
        int index = fragmentManager.getBackStackEntryCount() - 1;
        if (index < 0) {
            return null;
        }
        FragmentManager.BackStackEntry backEntry = fragmentManager.getBackStackEntryAt(index);
        return backEntry.getName();
    }

    public static void loadFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment, tag);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    public static void removeFragment(String tag) {
        fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public static void hideActionBar(ActionBar actionBar, Toolbar toolbar, int mAnimDuration) {
        if (mToolbarHeight == 0) {
            mToolbarHeight = toolbar.getHeight();
        }

        if (mVaActionBar != null && mVaActionBar.isRunning()) {
            return;
        }

        mVaActionBar = ValueAnimator.ofInt(mToolbarHeight, 0);
        mVaActionBar.addUpdateListener(animation -> {
            ((AppBarLayout.LayoutParams) toolbar.getLayoutParams()).height
                    = (Integer) animation.getAnimatedValue();
            toolbar.requestLayout();
        });

        mVaActionBar.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                if (actionBar != null) {
                    actionBar.hide();
                }
            }
        });

        mVaActionBar.setDuration(mAnimDuration);
        mVaActionBar.start();
    }

    public static void showActionBar(ActionBar actionBar, Toolbar toolbar, int mAnimDuration) {
        if (mVaActionBar != null && mVaActionBar.isRunning()) {
            return;
        }

        mVaActionBar = ValueAnimator.ofInt(0, mToolbarHeight);
        mVaActionBar.addUpdateListener(animation -> {
            ((AppBarLayout.LayoutParams) toolbar.getLayoutParams()).height
                    = (Integer) animation.getAnimatedValue();
            toolbar.requestLayout();
        });

        mVaActionBar.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

                if (actionBar != null) {
                    actionBar.show();
                }
            }
        });

        mVaActionBar.setDuration(mAnimDuration);
        mVaActionBar.start();
    }

}