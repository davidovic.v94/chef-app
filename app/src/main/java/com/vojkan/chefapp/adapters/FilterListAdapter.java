package com.vojkan.chefapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.viewholders.CheckboxListViewHolder;
import com.vojkan.chefapp.adapters.viewholders.ChipListViewHolder;
import com.vojkan.chefapp.adapters.viewholders.SliderListViewHolder;
import com.vojkan.chefapp.recipedata.Filter;

import java.util.List;

public class FilterListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_CUISINE = 0, VIEW_TYPE_INTOLERANCE = 1, VIEW_TYPE_DIET = 2, VIEW_TYPE_MEAL_TYPE = 3, VIEW_TYPE_NUTRIENTS = 4;
    private final Activity activity;
    private final List<Filter> filerList;

    public FilterListAdapter(Activity activity, List<Filter> filerList) {
        this.filerList = filerList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_CUISINE) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.chip_group_layout, parent, false);
            return new ChipListViewHolder(view, activity);
        } else if (viewType == VIEW_TYPE_INTOLERANCE) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.checkbox_group_layout, parent, false);
            return new CheckboxListViewHolder(view, activity);
        } else if (viewType == VIEW_TYPE_DIET) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.chip_group_layout, parent, false);
            return new ChipListViewHolder(view, activity);
        } else if (viewType == VIEW_TYPE_MEAL_TYPE) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.checkbox_group_layout, parent, false);
            return new CheckboxListViewHolder(view, activity);
        } else {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.slider_group_layout, parent, false);
            return new SliderListViewHolder(view, activity);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ChipListViewHolder) {
            Filter filter = filerList.get(position);
            ChipListViewHolder chipListViewHolder = (ChipListViewHolder) holder;

            chipListViewHolder.bind(filter);

            chipListViewHolder.itemView.setOnClickListener(v -> {
                boolean expanded = filter.isExpanded();
                filter.setExpanded(!expanded);
                notifyItemChanged(position);
            });
        } else if (holder instanceof CheckboxListViewHolder) {
            Filter filter = filerList.get(position);
            CheckboxListViewHolder checkboxListViewHolder = (CheckboxListViewHolder) holder;

            checkboxListViewHolder.bind(filter);

            checkboxListViewHolder.itemView.setOnClickListener(v -> {
                boolean expanded = filter.isExpanded();
                filter.setExpanded(!expanded);
                notifyItemChanged(position);
            });
        } else if (holder instanceof SliderListViewHolder) {
            Filter filter = filerList.get(position);
            SliderListViewHolder sliderListViewHolder = (SliderListViewHolder) holder;

            sliderListViewHolder.bind(filter);

            sliderListViewHolder.itemView.setOnClickListener(v -> {
                boolean expanded = filter.isExpanded();
                filter.setExpanded(!expanded);
                notifyItemChanged(position);
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (filerList.get(position).getTitle()) {
            case "Cuisine":
                return VIEW_TYPE_CUISINE;
            case "Intolerance":
                return VIEW_TYPE_INTOLERANCE;
            case "Diet":
                return VIEW_TYPE_DIET;
            case "Meal Type":
                return VIEW_TYPE_MEAL_TYPE;
            case "Macro And Micro Nutrients":
                return VIEW_TYPE_NUTRIENTS;
            default:
                return 0;
        }
    }

    @Override
    public int getItemCount() {
        return filerList == null ? 0 : filerList.size();
    }

}
