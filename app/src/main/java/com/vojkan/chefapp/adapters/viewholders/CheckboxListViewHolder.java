package com.vojkan.chefapp.adapters.viewholders;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.recipedata.Filter;
import com.vojkan.chefapp.utils.StringFormatter;

import java.util.ArrayList;
import java.util.List;

public class CheckboxListViewHolder extends RecyclerView.ViewHolder {

    private final Activity activity;
    private final TextView title;
    private final LayoutInflater layoutInflater;
    private List<Object> intoleranceList;
    private List<Object> mealTypeList;
    public static final List<String> checkedIntolerancesList = new ArrayList<>();
    public static final List<String> checkedMealTypesList = new ArrayList<>();


    public CheckboxListViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
        layoutInflater = LayoutInflater.from(activity);
        title = itemView.findViewById(R.id.filter_group_title);
    }

    public void bind(Filter filter) {
        title.setText(filter.getTitle());

        switch (filter.getTitle()) {
            case "Intolerance":
                title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.intolerance_icon, 0, R.drawable.ic_baseline_keyboard_arrow_down_24, 0);
                setIntoleranceCheckboxes(filter);
                break;
            case "Meal Type":
                title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.meal_type, 0, R.drawable.ic_baseline_keyboard_arrow_down_24, 0);
                setMealTypeCheckboxes(filter);
                break;
        }
        title.setCompoundDrawablePadding((int) (activity.getResources().getDisplayMetrics().density * 16));
    }

    private void setIntoleranceCheckboxes(Filter filter) {
        LinearLayout intoleranceCheckboxGroup = itemView.findViewById(R.id.checkbox_item_holder);
        intoleranceCheckboxGroup.setVisibility(filter.isExpanded() ? View.VISIBLE : View.GONE);

        if (intoleranceList == null) {
            intoleranceList = filter.getList();

            for (int i = 0; i < intoleranceList.size(); i++) {
                String name = StringFormatter.capitalizeFirstLetter(intoleranceList.get(i).toString());
                MaterialCheckBox checkBox = (MaterialCheckBox) layoutInflater.inflate(R.layout.checkbox_item, intoleranceCheckboxGroup, false);
                checkBox.setText(name);
                checkBox.setId(i);
                intoleranceCheckboxGroup.addView(checkBox);

                checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                    CheckBox c = (CheckBox) compoundButton;
                    if (b) {
                        checkedIntolerancesList.add(StringFormatter.formatStringForRequest(c.getText().toString()));
                    } else {
                        checkedIntolerancesList.remove(StringFormatter.formatStringForRequest(c.getText().toString()));
                    }
                });
            }
        }
    }

    private void setMealTypeCheckboxes(Filter filter) {
        LinearLayout mealTypeCheckboxGroup = itemView.findViewById(R.id.checkbox_item_holder);
        mealTypeCheckboxGroup.setVisibility(filter.isExpanded() ? View.VISIBLE : View.GONE);

        if (mealTypeList == null) {
            mealTypeList = filter.getList();

            for (int i = 0; i < mealTypeList.size(); i++) {
                String name = StringFormatter.capitalizeFirstLetter(mealTypeList.get(i).toString());
                MaterialCheckBox checkBox = (MaterialCheckBox) layoutInflater.inflate(R.layout.checkbox_item, mealTypeCheckboxGroup, false);
                checkBox.setText(name);
                checkBox.setId(i);
                mealTypeCheckboxGroup.addView(checkBox);

                checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                    CheckBox c = (CheckBox) compoundButton;
                    if (b) {
                        checkedMealTypesList.add(StringFormatter.formatStringForRequest(c.getText().toString()));
                    } else {
                        checkedMealTypesList.remove(StringFormatter.formatStringForRequest(c.getText().toString()));
                    }
                });
            }
        }
    }

}
