package com.vojkan.chefapp.adapters.viewholders;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.slider.RangeSlider;
import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.recipedata.Filter;
import com.vojkan.chefapp.utils.StringFormatter;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class SliderListViewHolder extends RecyclerView.ViewHolder {

    private final Activity activity;
    private final TextView title;
    private final LayoutInflater layoutInflater;
    private List<Object> nutrientList;
    private Float minValue;
    private Float maxValue;
    private final Map<String, Float> selectedNutrients = new HashMap<>();

    public SliderListViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
        layoutInflater = LayoutInflater.from(activity);
        title = itemView.findViewById(R.id.filter_group_title);
    }

    public void bind(Filter filter) {
        title.setText(filter.getTitle());
        title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.calories_icon, 0, R.drawable.ic_baseline_keyboard_arrow_down_24, 0);
        title.setCompoundDrawablePadding((int) (activity.getResources().getDisplayMetrics().density * 16));

        LinearLayout nutrientSliderGroup = itemView.findViewById(R.id.slider_item_holder);
        nutrientSliderGroup.setVisibility(filter.isExpanded() ? View.VISIBLE : View.GONE);

        if (nutrientList == null) {
            nutrientList = filter.getList();

            for (int i = 0; i < nutrientList.size(); i++) {
                String name = StringFormatter.capitalizeEveryWord(nutrientList.get(i).toString());
                LinearLayout sliderLayout = (LinearLayout) layoutInflater.inflate(R.layout.two_side_slider_item, nutrientSliderGroup, false);
                TextView titleTextView = sliderLayout.findViewById(R.id.slider_item_title);
                RangeSlider rangeSlider = sliderLayout.findViewById(R.id.slider_item_slider);
                titleTextView.setText(name);

                if (nutrientList.get(i).toString().equals("CARBOHYDRATES") || nutrientList.get(i).toString().equals("PROTEIN") ||
                        nutrientList.get(i).toString().equals("FAT") || nutrientList.get(i).toString().equals("SATURATED_FAT") ||
                        nutrientList.get(i).toString().equals("FIBER") || nutrientList.get(i).toString().equals("SUGAR")) {
                    rangeSlider.setLabelFormatter(value -> String.format(Locale.getDefault(), "%d grams", (int) value));
                } else if (nutrientList.get(i).toString().equals("VITAMIN_D") || nutrientList.get(i).toString().equals("VITAMIN_K") ||
                        nutrientList.get(i).toString().equals("VITAMIN_B12") || nutrientList.get(i).toString().equals("VITAMIN_B9") ||
                        nutrientList.get(i).toString().equals("FOLIC_ACID") || nutrientList.get(i).toString().equals("IODINE") ||
                        nutrientList.get(i).toString().equals("SELENIUM")) {
                    rangeSlider.setLabelFormatter(value -> String.format(Locale.getDefault(), "%d micrograms", (int) value));
                } else if (nutrientList.get(i).toString().equals("VITAMIN_A")) {
                    rangeSlider.setLabelFormatter(value -> String.format(Locale.getDefault(), "%d IU", (int) value));
                } else {
                    rangeSlider.setLabelFormatter(value -> String.format(Locale.getDefault(), "%d milligrams", (int) value));
                }

                rangeSlider.addOnChangeListener((slider, value, fromUser) -> {
                    minValue = slider.getValues().get(0);
                    maxValue = slider.getValues().get(1);
                    if (minValue != slider.getValueFrom()) {
                        selectedNutrients.put("min" + StringFormatter.clearWhiteSpaces(name), minValue);
                    }
                    if (maxValue != slider.getValueTo()) {
                        selectedNutrients.put("max" + StringFormatter.clearWhiteSpaces(name), maxValue);
                    }
                    if (MainActivity.nutrition.length() != 0) {
                        MainActivity.nutrition.delete(0, MainActivity.nutrition.length());
                    }
                    getValuesFromMap();
                });

                nutrientSliderGroup.addView(sliderLayout);
            }
        }
    }

    private void getValuesFromMap() {
        Set<String> keys = selectedNutrients.keySet();
        for (String key : keys) {
            MainActivity.nutrition.append("&").append(key).append("=").append(selectedNutrients.get(key));
        }
    }

}
