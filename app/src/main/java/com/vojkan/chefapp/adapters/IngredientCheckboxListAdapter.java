package com.vojkan.chefapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.squareup.picasso.Picasso;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.recipedata.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class IngredientCheckboxListAdapter extends RecyclerView.Adapter<IngredientCheckboxListAdapter.IngredientListViewHolder> implements Filterable {

    private final Context context;
    private final MaterialCardView cardView;
    private final MaterialButton addButton;
    private final List<Ingredient> list;
    private final List<Ingredient> listFull;
    private final List<Ingredient> selectedIngredients = new ArrayList<>();

    public IngredientCheckboxListAdapter(Context context, MaterialCardView cardView, List<Ingredient> list, MaterialButton addButton) {
        this.context = context;
        this.list = list;
        this.cardView = cardView;
        this.addButton = addButton;
        listFull = new ArrayList<>(list);
    }

    public static class IngredientListViewHolder extends RecyclerView.ViewHolder {

        private final MaterialCheckBox checkBox;
        private final ImageView ingredientIcon;
        private final ChipGroup chipGroup;

        public IngredientListViewHolder(@NonNull View itemView, MaterialCardView cardView) {
            super(itemView);
            chipGroup = cardView.findViewById(R.id.ingredients_chip_group);
            checkBox = itemView.findViewById(R.id.checkbox_item);
            ingredientIcon = itemView.findViewById(R.id.ingredient_icon);
            TextView ingredientName = itemView.findViewById(R.id.ingredient__name);
            ingredientName.setVisibility(View.GONE);

            itemView.setOnClickListener(view -> checkBox.setChecked(!checkBox.isChecked()));

        }

        private void bind(Context context, Ingredient ingredient, List<Ingredient> selectedIngredients,
                          MaterialCardView cardView, MaterialButton addButton) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(ingredient.isChecked());
            checkBox.setText(ingredient.getName());
            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                CheckBox checkBox1 = (CheckBox) compoundButton;
                if (b) {
                    ingredient.setChecked(true);
                    checkBox1.setChecked(true);
                    selectedIngredients.add(ingredient);

                    Chip chip = new Chip(context);
                    chip.setText(checkBox1.getText().toString());
                    chip.setCheckable(false);
                    chip.setClickable(false);
                    chip.setCloseIconVisible(true);
                    chip.setOnCloseIconClickListener(view -> {
                        removeChip(chip.getText().toString());
                        if (chipGroup.getChildCount() == 0) {
                            cardView.setVisibility(View.GONE);
                            addButton.setVisibility(View.GONE);
                        }
                        ingredient.setChecked(false);
                        selectedIngredients.remove(ingredient);
                        if (checkBox1.getText().toString().equals(chip.getText().toString())) {
                            checkBox1.setChecked(false);
                        }
                    });
                    chipGroup.addView(chip);
                } else {
                    ingredient.setChecked(false);
                    checkBox1.setChecked(false);
                    selectedIngredients.remove(ingredient);
                    removeChip(ingredient.getName());
                }

                if (chipGroup.getChildCount() == 0) {
                    cardView.setVisibility(View.GONE);
                    addButton.setVisibility(View.GONE);
                } else {
                    cardView.setVisibility(View.VISIBLE);
                    addButton.setVisibility(View.VISIBLE);
                }
            });
            Picasso.get().load("https://spoonacular.com/cdn/ingredients_100x100/" + ingredient.getImageFile())
                    .fit()
                    .centerInside()
                    .error(R.drawable.no_image_available_image)
                    .into(ingredientIcon);
        }

        private void removeChip(String chipText) {
            for (int i = 0; i < chipGroup.getChildCount(); i++) {
                Chip chip = (Chip) chipGroup.getChildAt(i);
                if (chip.getText().equals(chipText)) {
                    chipGroup.removeView(chip);
                    break;
                }
            }
        }

    }

    @NonNull
    @Override
    public IngredientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_list_item, parent, false);
        return new IngredientListViewHolder(v, cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientListViewHolder holder, int position) {
        final Ingredient ingredient = list.get(position);
        holder.bind(context, ingredient, selectedIngredients, cardView, addButton);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return testFilter;
    }

    private final Filter testFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Ingredient> filteredList = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0) {
                filteredList.addAll(listFull);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (Ingredient ingredient : listFull) {
                    if (ingredient.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(ingredient);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (list != null) {
                list.clear();
            }
            list.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public List<Ingredient> getSelectedIngredients() {
        return selectedIngredients;
    }

}
