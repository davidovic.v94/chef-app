package com.vojkan.chefapp.adapters.viewholders;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.recipedata.Filter;
import com.vojkan.chefapp.utils.StringFormatter;

import java.util.ArrayList;
import java.util.List;

public class ChipListViewHolder extends RecyclerView.ViewHolder {

    private final Activity activity;
    private final TextView title;
    private final LayoutInflater layoutInflater;
    private List<Object> cuisineList;
    private List<Object> dietList;
    public static final List<String> checkedDietsList = new ArrayList<>();
    public static final List<String> checkedCuisinesList = new ArrayList<>();


    public ChipListViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
        layoutInflater = LayoutInflater.from(activity);
        title = itemView.findViewById(R.id.filter_group_title);
    }

    public void bind(Filter filter) {
        title.setText(filter.getTitle());

        switch (filter.getTitle()) {
            case "Cuisine":
                title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.flag_icon, 0, R.drawable.ic_baseline_keyboard_arrow_down_24, 0);
                setCuisineChips(filter);
                break;
            case "Diet":
                title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.diet_icon, 0, R.drawable.ic_baseline_keyboard_arrow_down_24, 0);
                setDietChips(filter);
                break;
        }

        title.setCompoundDrawablePadding((int) (activity.getResources().getDisplayMetrics().density * 16));

    }

    private void setCuisineChips(Filter filter) {
        ChipGroup cuisineChipGroup = itemView.findViewById(R.id.chip_group);
        cuisineChipGroup.setVisibility(filter.isExpanded() ? View.VISIBLE : View.GONE);

        if (cuisineList == null) {
            cuisineList = filter.getList();

            for (int i = 0; i < cuisineList.size(); i++) {
                String name = StringFormatter.capitalizeEveryWord(cuisineList.get(i).toString());
                Chip chip = (Chip) layoutInflater.inflate(R.layout.chip_item, cuisineChipGroup, false);
                chip.setText(name);
                chip.setId(i);
                cuisineChipGroup.addView(chip);

                chip.setOnCheckedChangeListener((compoundButton, b) -> {
                    Chip c = (Chip) compoundButton;
                    if (b) {
                        checkedCuisinesList.add(StringFormatter.formatStringForRequest(c.getText().toString()));
                    } else {
                        checkedCuisinesList.remove(StringFormatter.formatStringForRequest(c.getText().toString()));
                    }
                });
            }

        }
    }

    private void setDietChips(Filter filter) {
        ChipGroup dietChipGroup = itemView.findViewById(R.id.chip_group);
        dietChipGroup.setVisibility(filter.isExpanded() ? View.VISIBLE : View.GONE);
        if (dietList == null) {
            dietList = filter.getList();

            for (int i = 0; i < dietList.size(); i++) {
                String name = StringFormatter.capitalizeFirstLetter(dietList.get(i).toString());
                Chip chip = (Chip) layoutInflater.inflate(R.layout.chip_item, dietChipGroup, false);
                chip.setText(name);
                chip.setId(i);
                dietChipGroup.addView(chip);

                chip.setOnCheckedChangeListener((compoundButton, b) -> {
                    Chip c = (Chip) compoundButton;
                    if (b) {
                        checkedDietsList.add(StringFormatter.formatStringForRequest(c.getText().toString()));
                    } else {
                        checkedDietsList.remove(StringFormatter.formatStringForRequest(c.getText().toString()));
                    }
                });
            }
        }
    }

}
