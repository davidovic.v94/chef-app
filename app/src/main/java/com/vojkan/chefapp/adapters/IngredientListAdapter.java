package com.vojkan.chefapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.squareup.picasso.Picasso;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.recipedata.Ingredient;

import java.util.List;

public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.IngredientListViewHolder> {

    private final List<Ingredient> ingredients;

    public IngredientListAdapter(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public static class IngredientListViewHolder extends RecyclerView.ViewHolder {

        private final TextView ingredientName;
        private final ImageView ingredientIcon;

        public IngredientListViewHolder(@NonNull View itemView) {
            super(itemView);
            MaterialCheckBox checkBox = itemView.findViewById(R.id.checkbox_item);
            ingredientIcon = itemView.findViewById(R.id.ingredient_icon);
            ingredientName = itemView.findViewById(R.id.ingredient__name);
            checkBox.setVisibility(View.GONE);
        }

    }

    @NonNull
    @Override
    public IngredientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_list_item, parent, false);
        return new IngredientListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientListViewHolder holder, int position) {
        final Ingredient ingredient = ingredients.get(position);
        String name = ingredient.getName();
        String imageUrl = "https://spoonacular.com/cdn/ingredients_100x100/" + ingredient.getImageFile();

        holder.ingredientName.setText(name);
        Picasso.get().load(imageUrl)
                .fit()
                .centerInside()
                .error(R.drawable.no_image_available_image)
                .into(holder.ingredientIcon);
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

}
