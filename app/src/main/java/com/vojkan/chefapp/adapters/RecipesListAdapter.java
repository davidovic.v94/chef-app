package com.vojkan.chefapp.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.viewholders.ListItemViewHolder;
import com.vojkan.chefapp.adapters.viewholders.LoadingViewHolder;
import com.vojkan.chefapp.data.DataManage;
import com.vojkan.chefapp.recipedata.Recipe;
import com.vojkan.chefapp.utils.CircleTransform;

import java.util.List;

public class RecipesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity activity;
    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
    private int currentItems, totalItems, scrollOutItems;
    private boolean isLoading;
    private final List<Recipe> recipesList;
    private OnItemClickListener onItemClickListener;
    private ILoadMore loadMore;
    private final DataManage dataManage = new DataManage(MainActivity.sharedPreferences);

    public interface ILoadMore {
        void onLoadMore();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public RecipesListAdapter(Activity activity, RecyclerView recyclerView, List<Recipe> recipesList) {
        this.activity = activity;
        this.recipesList = recipesList;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (linearLayoutManager != null) {
                    currentItems = linearLayoutManager.getChildCount();
                    totalItems = linearLayoutManager.getItemCount();
                    scrollOutItems = linearLayoutManager.findLastVisibleItemPosition();
                }

                if (!isLoading && (totalItems == (currentItems + scrollOutItems))) {
                    if (loadMore != null) {
                        loadMore.onLoadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_layout, parent, false);
            return new ListItemViewHolder(view, onItemClickListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_loading_layout, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return recipesList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListItemViewHolder) {
            Recipe recipe = recipesList.get(position);
            ListItemViewHolder listItemViewHolder = (ListItemViewHolder) holder;

            if (recipe.getName() != null) {
                listItemViewHolder.title.setText(recipe.getName());
            }
            if (recipe.getSummary() != null) {
                listItemViewHolder.description.setText(recipe.getSummary());
            } else {
                listItemViewHolder.description.setVisibility(View.GONE);
            }
            if (MainActivity.favoriteRecipes.contains(recipe)) {
                listItemViewHolder.favoriteIcon.setImageResource(R.drawable.ic_baseline_favorite_24);
                listItemViewHolder.isFavorite = true;
            }
            listItemViewHolder.favoriteIcon.setOnClickListener(view -> {
                if (listItemViewHolder.isFavorite) {
                    listItemViewHolder.favoriteIcon.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    MainActivity.favoriteRecipes.remove(recipe);
                    listItemViewHolder.isFavorite = false;
                } else {
                    listItemViewHolder.favoriteIcon.setImageResource(R.drawable.ic_baseline_favorite_24);
                    MainActivity.favoriteRecipes.add(recipe);
                    listItemViewHolder.isFavorite = true;
                }
                dataManage.saveData();
            });
            listItemViewHolder.shareIcon.setOnClickListener(view -> {
                Intent shareIntent = new Intent();
                shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Recipe link");
                shareIntent.putExtra(Intent.EXTRA_TEXT, recipe.getRecipeUrl());
                shareIntent.setType("text/plain");
                activity.startActivity(shareIntent);
            });
            Picasso.get().load(recipe.getImageUrl())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.no_image_available_image)
//                    .placeholder(R.drawable.no_image_available_image)
                    .transform(new CircleTransform(50, 0))
                    .into(listItemViewHolder.recipeImage);
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return recipesList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

}
