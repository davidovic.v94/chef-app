package com.vojkan.chefapp.adapters.viewholders;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vojkan.chefapp.R;
import com.vojkan.chefapp.adapters.RecipesListAdapter;

public class ListItemViewHolder extends RecyclerView.ViewHolder {

    public ImageView recipeImage, favoriteIcon, shareIcon;
    public TextView title, description;
    public boolean isFavorite;

    public ListItemViewHolder(@NonNull View itemView, RecipesListAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);
        recipeImage = itemView.findViewById(R.id.list_item_image_view);
        favoriteIcon = itemView.findViewById(R.id.favorite_image_view);
        shareIcon = itemView.findViewById(R.id.share_image_view);
        title = itemView.findViewById(R.id.list_item_title);
        description = itemView.findViewById(R.id.list_item_description);

        itemView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onItemClickListener.onItemClick(position);
                }
            }
        });
    }
}
