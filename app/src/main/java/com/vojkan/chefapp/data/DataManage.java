package com.vojkan.chefapp.data;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vojkan.chefapp.MainActivity;
import com.vojkan.chefapp.recipedata.Ingredient;
import com.vojkan.chefapp.recipedata.Recipe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataManage implements IData {

    SharedPreferences sharedPreferences;

    public DataManage(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void saveData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        try {
            List<String> ingredientTitles = new ArrayList<>();
            List<String> ingredientImages = new ArrayList<>();
            List<Recipe> favorites = new ArrayList<>(MainActivity.favoriteRecipes);
            List<String> favoritesJson = new ArrayList<>();

            for (Ingredient ingredient : MainActivity.ingredients) {
                ingredientTitles.add(ingredient.getName());
                ingredientImages.add(ingredient.getImageFile());
            }

            for (Recipe recipe : favorites) {
                favoritesJson.add(new Gson().toJson(recipe));
            }

            editor.putString("favoriteRecipes", ObjectSerializer.serialize((Serializable) favoritesJson)).apply();
            editor.putString("ingredientTitles", ObjectSerializer.serialize((Serializable) ingredientTitles)).apply();
            editor.putString("ingredientImages", ObjectSerializer.serialize((Serializable) ingredientImages)).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadData() {
        try {
            List<String> favoritesJson;
            List<String> ingredientTitles;
            List<String> ingredientImages;

            MainActivity.ingredients.clear();
            MainActivity.favoriteRecipes.clear();

            favoritesJson = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("favoriteRecipes", ObjectSerializer.serialize(new ArrayList<String>())));
            ingredientTitles = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("ingredientTitles", ObjectSerializer.serialize(new ArrayList<String>())));
            ingredientImages = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("ingredientImages", ObjectSerializer.serialize(new ArrayList<String>())));

            if (favoritesJson != null) {
                if (favoritesJson.size() > 0) {
                    for (String s : favoritesJson) {
                        MainActivity.favoriteRecipes.add(new Gson().fromJson(s, new TypeToken<Recipe>(){}.getType()));
                    }
                }
            }

            if (ingredientTitles != null && ingredientImages != null) {
                if (ingredientTitles.size() > 0 && ingredientImages.size() > 0) {
                    if (ingredientTitles.size() == ingredientImages.size()) {
                        for (int i = 0; i < ingredientTitles.size(); i++) {
                            MainActivity.ingredients.add(new Ingredient(ingredientTitles.get(i), ingredientImages.get(i)));
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
