package com.vojkan.chefapp.data;

public interface IData {

    void saveData();

    void loadData();

}
